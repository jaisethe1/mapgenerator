﻿//using UnityEngine;
//using UnityEditor;
//using System.Collections.Generic;

//public class FindMissingScriptsRecursively : EditorWindow
//{
//    static int go_count = 0, components_count = 0, missing_count = 0;

//    [MenuItem("MyMenu/SelectMissing")]
//    static void SelectMissing(MenuCommand command)
//    {
//        Transform[] ts = FindObjectsOfType<Transform>();
//        List<GameObject> selection = new List<GameObject>();
//        foreach (Transform t in ts)
//        {
//            Component[] cs = t.gameObject.GetComponents<Component>();
//            foreach (Component c in cs)
//            {
//                if (c == null)
//                {
//                    selection.Add(t.gameObject);
//                }
//            }
//        }
//        Selection.objects = selection.ToArray();
//    }

//    [MenuItem("Auto/Remove Missing Scripts Recursively")]
//    private static void FindAndRemoveMissingInSelected()
//    {
//        var deepSelection = EditorUtility.CollectDeepHierarchy(Selection.gameObjects);
//        int compCount = 0;
//        int goCount = 0;
//        foreach (var o in deepSelection)
//        {
//            if (o is GameObject go)
//            {
//                int count = GameObjectUtility.GetMonoBehavioursWithMissingScriptCount(go);
//                if (count > 0)
//                {
//                    // Edit: use undo record object, since undo destroy wont work with missing
//                    Undo.RegisterCompleteObjectUndo(go, "Remove missing scripts");
//                    GameObjectUtility.RemoveMonoBehavioursWithMissingScript(go);
//                    compCount += count;
//                    goCount++;
//                }
//            }
//        }
//        Debug.Log($"Found and removed {compCount} missing scripts from {goCount} GameObjects");
//    }

//    [MenuItem("Edit/Cleanup Missing Scripts")]
//    static void CleanupMissingScripts()
//    {
//        for (int i = 0; i < Selection.gameObjects.Length; i++)
//        {
//            var gameObject = Selection.gameObjects[i];

//            // We must use the GetComponents array to actually detect missing components
//            var components = gameObject.GetComponents<Component>();

//            // Create a serialized object so that we can edit the component list
//            var serializedObject = new SerializedObject(gameObject);
//            // Find the component list property
//            var prop = serializedObject.FindProperty("m_Component");

//            // Track how many components we've removed
//            int r = 0;

//            // Iterate over all components
//            for (int j = 0; j < components.Length; j++)
//            {
//                // Check if the ref is null
//                if (components[j] == null)
//                {
//                    // If so, remove from the serialized component array
//                    prop.DeleteArrayElementAtIndex(j - r);
//                    // Increment removed count
//                    r++;
//                }
//            }

//            // Apply our changes to the game object
//            serializedObject.ApplyModifiedProperties();
//        }
//    }

//    [MenuItem("Window/FindMissingScriptsRecursively")]
//    public static void ShowWindow()
//    {
//        EditorWindow.GetWindow(typeof(FindMissingScriptsRecursively));
//    }

//    public void OnGUI()
//    {
//        if (GUILayout.Button("Find Missing Scripts in selected GameObjects"))
//        {
//            FindInSelected();
//        }
//    }
//    private static void FindInSelected()
//    {
//        GameObject[] go = Selection.gameObjects;
//        go_count = 0;
//        components_count = 0;
//        missing_count = 0;
//        foreach (GameObject g in go)
//        {
//            FindInGO(g);
//        }
//        Debug.Log(string.Format("Searched {0} GameObjects, {1} components, found {2} missing", go_count, components_count, missing_count));
//    }

//    private static void FindInGO(GameObject g)
//    {
//        go_count++;
//        Component[] components = g.GetComponents<Component>();
//        for (int i = 0; i < components.Length; i++)
//        {
//            components_count++;
//            if (components[i] == null)
//            {
//                missing_count++;
//                string s = g.name;
//                Transform t = g.transform;
//                while (t.parent != null)
//                {
//                    s = t.parent.name + "/" + s;
//                    t = t.parent;
//                }
//                Debug.Log(s + " has an empty script attached in position: " + i, g);
//                DestroyImmediate(components[i]);
//            }
//        }
//        // Now recurse through each child GO (if there are any):
//        foreach (Transform childT in g.transform)
//        {
//            //Debug.Log("Searching " + childT.name  + " " );
//            FindInGO(childT.gameObject);
//        }
//    }
//}