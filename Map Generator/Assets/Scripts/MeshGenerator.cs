﻿using System.Collections.Generic;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// A class that allows the creation of mesh data based on map data.
    /// </summary>
    public static class MeshGenerator
    {
        /// <summary>
        /// Generates a terrain mesh based on the tile map as well as desired multiplier, level of detail, and height curve.
        /// </summary>
        /// <param name="tileMap">The list of tiles' data that correlate to the desired map to create.</param>
        /// <param name="terrainChunkIndex">The index of the terrain chunk we are creating.</param>
        /// <param name="terrainChunkSize">The size of the terrain chunk so we know which portion of tiles to reference</param>
        /// <param name="heightMultiplier">The amount to multiply the heights by in order to exaggerate them.</param>
        /// <param name="levelOfDetail">The level of detail to generate the mesh at. Low number = more polys.0</param>
        /// <param name="heightCurve">How to affect the heights based on a curve.</param>
        /// <returns>The mesh data generated based on input params.</returns>
        public static MeshData GenerateTerrainMesh(ref Tile[,] tileMap, Vector2 terrainChunkIndex, int terrainChunkSize, 
            float heightMultiplier, int levelOfDetail, AnimationCurve heightCurve = null)
        {
            int width = terrainChunkSize;
            int height = terrainChunkSize;
            float topLeftX = (width - 1) / -2f;
            float topLeftZ = (height - 1) / 2f;

            //  BUG/HACK: 
            //      There is an issue with Unity where sometimes when an animation curve is passed 
            //      as a parameter, it can become corrupted and when Evaluate is called on it, it returns
            //      some crazy, out of whack numbers. The only solution I have found to fix this issue
            //      is to re-create the curve variable with the same keys as the param.
            AnimationCurve hCurve = new AnimationCurve(heightCurve.keys);

            int meshSimplificationIncrement = (levelOfDetail == 0) ? 1 : levelOfDetail * 2;
            int verticesPerLine = (width - 1) / meshSimplificationIncrement + 1;

            List<int> topEdgeVertexIndexes = new List<int>();
            List<int> bottomEdgeVertexIndexes = new List<int>();
            List<int> leftEdgeVertexIndexes = new List<int>();
            List<int> rightEdgeVertexIndexes = new List<int>();

            MeshData meshData = new MeshData(verticesPerLine);
            int vertexIndex = 0;

            for (int x = 0; x < width; x += meshSimplificationIncrement)
            {
                for (int y = 0; y < height; y += meshSimplificationIncrement)
                {
                    // capture the vertex indexes for the given sides of the chunk
                    {
                        if (x == 0) 
                            leftEdgeVertexIndexes.Add(vertexIndex);
                        if (x == width - 1)
                            rightEdgeVertexIndexes.Add(vertexIndex);
                        if (y == 0)
                            bottomEdgeVertexIndexes.Add(vertexIndex);
                        if (y == height - 1)
                            topEdgeVertexIndexes.Add(vertexIndex);
                    }

                    // calculate the x/y index in the tile-map based on the size and index of the chunk
                    // to reference the area of the tile-map specific to 'this' chunk.
                    int xMapIndex = x + (terrainChunkSize * (int)terrainChunkIndex.x);
                    int yMapIndex = y + (terrainChunkSize * (int)terrainChunkIndex.y);

                    {
                        // modify the reference indices to be one left (x) and/or one down (y)
                        // so that the mesh heights align properly for all terrain chunks
                        // after the zero-th row/column. I.E. index x or y = 0
                        if ((int)terrainChunkIndex.x > 0)
                        {
                            xMapIndex = xMapIndex - 1;
                        }
                        if ((int)terrainChunkIndex.y > 0)
                        {
                            yMapIndex = yMapIndex - 1;
                        }
                    }

                    float heightVal = 0;
                    heightVal = tileMap[xMapIndex, yMapIndex].heightValue;
                    if (heightCurve != null)
                    {
                        heightVal = hCurve.Evaluate(heightVal);
                    }

                    heightVal = heightVal * heightMultiplier;
                    meshData.vertices[vertexIndex] = new Vector3(topLeftX + x, heightVal, topLeftZ + y);
                    meshData.uvs[vertexIndex] = new Vector2(x / (float)width, y / (float)height);

                    if (x < width - 1 && y < height - 1)
                    {
                        meshData.AddTriangle(vertexIndex, vertexIndex + verticesPerLine + 1, vertexIndex + verticesPerLine);
                        meshData.AddTriangle(vertexIndex + verticesPerLine + 1, vertexIndex, vertexIndex + 1);
                    }

                    vertexIndex++;
                }
            }

            meshData.topEdgeVertexIndexes = topEdgeVertexIndexes.ToArray();
            meshData.bottomEdgeVertexIndexes = bottomEdgeVertexIndexes.ToArray();
            meshData.leftEdgeVertexIndexes = leftEdgeVertexIndexes.ToArray();
            meshData.rightEdgeVertexIndexes = rightEdgeVertexIndexes.ToArray();

            // Unity, by default, calculates normals based on tri's. We want to calculate
            // normals based on vertex because our meshes will be aligned together and makes
            // it easier to normalize the mesh edges if the normals are created this way.
            meshData.CalculateVertexNormals();

            return meshData;
        }
    }

    /// <summary>
    /// Stores the data relative to the mesh such as: verts, tris, and uv's.
    /// </summary>
    public class MeshData
    {
        /// <summary>
        /// A list of vertices that make up the mesh.
        /// </summary>
        public Vector3[] vertices;

        /// <summary>
        /// The list of triangles of the mesh.
        /// </summary>
        public int[] triangles;

        /// <summary>
        /// The UV coords of the mesh so a texture can be applied appropriately.
        /// </summary>
        public Vector2[] uvs;

        /// <summary>
        /// The list of mesh's normals.
        /// </summary>
        public Vector3[] normals;

        /// <summary>
        /// A counter index for the current triangle so new ones can be added in appropriate order.
        /// </summary>
        private int triangleIndex;

        /// <summary>
        /// The indexes for all of the top-edge vertices.
        /// </summary>
        public int[] topEdgeVertexIndexes;

        /// <summary>
        /// The indexes for all of the bottom-edge vertices.
        /// </summary>
        public int[] bottomEdgeVertexIndexes;

        /// <summary>
        /// The indexes for all of the left-edge vertices.
        /// </summary>
        public int[] leftEdgeVertexIndexes;

        /// <summary>
        /// The indexes for all of the right-edge vertices.
        /// </summary>
        public int[] rightEdgeVertexIndexes;

        /// <summary>
        /// Generates initial structure of the mesh data.
        /// </summary>
        /// <param name="verticesPerLine">The number of vertices per line which translates into length/width of the mesh.</param>
        public MeshData(int verticesPerLine)
        {
            vertices = new Vector3[verticesPerLine * verticesPerLine];
            uvs = new Vector2[verticesPerLine * verticesPerLine];
            triangles = new int[(verticesPerLine - 1) * (verticesPerLine - 1) * 6];
        }

        /// <summary>
        /// Adds a triangle at values a, b, c.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public void AddTriangle(int a, int b, int c)
        {
            triangles[triangleIndex] = a;
            triangles[triangleIndex + 1] = b;
            triangles[triangleIndex + 2] = c;
            triangleIndex += 3;
        }

        /// <summary>
        /// Calculates the normals at each vertex of the mesh.
        /// </summary>
        /// <returns>An array of normals in the order of vertices.</returns>
        public Vector3[] CalculateVertexNormals()
        {
            Vector3[] vertexNormals = new Vector3[vertices.Length];
            int triangleCount = triangles.Length / 3;
            for(int iTri = 0; iTri < triangleCount; ++iTri)
            {
                int normalTriangleIndex = iTri * 3;
                int vertexIndexA = triangles[normalTriangleIndex];
                int vertexIndexB = triangles[normalTriangleIndex + 1];
                int vertexIndexC = triangles[normalTriangleIndex + 2];

                Vector3 triangleNormal = SurfaceNormalFromIndicies(vertexIndexA, vertexIndexB, vertexIndexC);
                vertexNormals[vertexIndexA] += triangleNormal;
                vertexNormals[vertexIndexB] += triangleNormal;
                vertexNormals[vertexIndexC] += triangleNormal;
            }

            for(int iVert = 0; iVert < vertexNormals.Length; ++iVert)
            {
                vertexNormals[iVert].Normalize();
            }

            normals = vertexNormals;

            return vertexNormals;
        }

        /// <summary>
        /// Calculates the surface normal of a triangle based on the average
        /// of its vertices' normals.
        /// </summary>
        /// <param name="indexA">Index of vertex A of the triangle.</param>
        /// <param name="indexB">Index of vertex B of the triangle.</param>
        /// <param name="indexC">Index of vertex C of the triangle.</param>
        /// <returns></returns>
        private Vector3 SurfaceNormalFromIndicies(int indexA, int indexB, int indexC)
        {
            Vector3 pointA = vertices[indexA];
            Vector3 pointB = vertices[indexB];
            Vector3 pointC = vertices[indexC];

            Vector3 sideAB = pointB - pointA;
            Vector3 sideAC = pointC - pointA;

            return Vector3.Cross(sideAB, sideAC).normalized;
        }

        /// <summary>
        /// Creates and returns a mesh generated based on the current data.
        /// </summary>
        /// <returns></returns>
        public Mesh CreateMesh()
        {
            Mesh mesh = new Mesh();

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uvs;
            mesh.normals = normals;

            return mesh;
        }
    }
}