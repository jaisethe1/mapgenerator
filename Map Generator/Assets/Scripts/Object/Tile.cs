﻿
using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Holds the data for a single tile within a map (IE pixel of an image).
    /// </summary>
    [Serializable]
    public class Tile
    {
        /// <summary>
        /// The x position within the map where the tile is located.
        /// </summary>
        public int x;

        /// <summary>
        /// The y position within the map where the tile is located.
        /// </summary>
        public int y;

        /// <summary>
        /// The height value of the tile.
        /// </summary>
        public float heightValue;

        /// <summary>
        /// The heat value of the tile.
        /// </summary>
        public float heatValue;

        /// <summary>
        /// The moisture value of the tile.
        /// </summary>
        public float moistureValue;

        /// <summary>
        /// The data for the tile's terrain type.
        /// </summary>
        public TerrainType terrainTypeData;

        /// <summary>
        /// The type of terrain the tile is.
        /// </summary>
        public TerrainTypes terrainTypeEnum;

        /// <summary>
        /// The data for the tile's heat type.
        /// </summary>
        public HeatType heatTypeData;

        /// <summary>
        /// The type of heat the tile is.
        /// </summary>
        public HeatTypes heatTypeEnum;

        /// <summary>
        /// The data for the tile's moisture type.
        /// </summary>
        public MoistureType moistureTypeData;

        /// <summary>
        /// The type of moisture the tile is.
        /// </summary>
        public MoistureTypes moistureTypeEnum;

        /// <summary>
        /// Tile's left neighbor.
        /// </summary>
        public Tile left;

        /// <summary>
        /// Tile's right neighbor.
        /// </summary>
        public Tile right;

        /// <summary>
        /// Tile's top neighbor.
        /// </summary>
        public Tile top;

        /// <summary>
        /// Tile's bottom neighbor.
        /// </summary>
        public Tile bottom;

        /// <summary>
        /// The tile's bitmask which marks the exact type of terrain
        /// the tile corresponds to. 
        /// </summary>
        /// <example>
        /// Based on a tile’s neighbors, we increment the bitmask as shown on the left side of the image below. 
        /// All possibilities are illustrated on the right side. Note that each value is unique. This allows us 
        /// to identify a block’s configuration very quickly.
        /// 
        ///                         0       [x]             8   [ ][x]
        ///                         
        ///                                 [ ]                    [ ] 
        ///                         1       [x]             9   [ ][x] 
        ///                         
        ///                         2       [x][ ]          10  [ ][x][ ]
        ///         
        ///         1                       [ ]                    [ ]
        ///      8 [ ] 2            3       [x][ ]          11  [ ][x][ ]
        ///         4
        ///                         4       [x]             12  [ ][x]
        ///                                 [ ]                    [ ]
        ///                                 
        ///                                 [ ]                    [ ]
        ///                         5       [x]             13  [ ][x]
        ///                                 [ ]                    [ ]
        ///                                 
        ///                         6       [x][ ]          14  [ ][x][ ]
        ///                                 [ ]                    [ ]
        ///                          
        ///                                 [ ]                    [ ]
        ///                         7       [x][ ]          15  [ ][x][ ]
        ///                                 [ ]                    [ ]
        /// 
        /// 
        /// The main benefit of bitmasking, is that you can then assign a texture, based on the bitmask value of each tile, 
        /// making your maps a lot prettier and way less blocky when done properly.
        /// 
        /// Another benefit of bitmasking, is that if a Tile’s bitmask value is not equal to 15, then we know it is an edge tile.
        /// </example>
        public int bitmask;

        /// <summary>
        /// Any tile that is NOT a WATER tile, is collidable.
        /// </summary>
        public bool collidable;

        /// <summary>
        /// Flag used by the flood fill algorithm to determine which tiles 
        /// have already been processed.
        /// </summary>
        public bool floodFilled;

        /// <summary>
        /// Default CTOR
        /// </summary>
        public Tile()
        {

        }

        /// <summary>
        /// Updates the bitmask of the tile.
        /// </summary>
        public void UpdateBitmask()
        {
            int count = 0;

            if (collidable &&  top != null && top.terrainTypeEnum == terrainTypeEnum)
                count += 1;
            if (collidable &&  right != null && right.terrainTypeEnum == terrainTypeEnum)
                count += 2;
            if (collidable &&  bottom != null && bottom.terrainTypeEnum == terrainTypeEnum)
                count += 4;
            if (collidable &&  left != null && left.terrainTypeEnum == terrainTypeEnum)
                count += 8;

            bitmask = count;
        }

        /// <summary>
        /// Finds the top tile neighbor of a given tile within a tile map.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="tiles"></param>
        /// <param name="tilesHeight"></param>
        /// <returns></returns>
        public static Tile GetTop(Tile t, ref Tile[,] tiles, int tilesHeight)
        {
            return tiles[t.x, MathHelper.Mod(t.y - 1, tilesHeight)];
        }

        /// <summary>
        /// Finds the bottom tile neighbor of a given tile within a tile map.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="tiles"></param>
        /// <param name="tilesHeight"></param>
        /// <returns></returns>
        public static Tile GetBottom(Tile t, ref Tile[,] tiles, int tilesHeight)
        {
            return tiles[t.x, MathHelper.Mod(t.y + 1, tilesHeight)];
        }

        /// <summary>
        /// Finds the left tile neighbor of a given tile within a tile map.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="tiles"></param>
        /// <param name="tilesWidth"></param>
        /// <returns></returns>
        public static Tile GetLeft(Tile t, ref Tile[,] tiles, int tilesWidth)
        {
            return tiles[MathHelper.Mod(t.x - 1, tilesWidth), t.y];
        }

        /// <summary>
        /// Finds the right tile neighbor of a given tile within a tile map.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="tiles"></param>
        /// <param name="tilesWidth"></param>
        /// <returns></returns>
        public static Tile GetRight(Tile t, ref Tile[,] tiles, int tilesWidth)
        {
            return tiles[MathHelper.Mod(t.x + 1, tilesWidth), t.y];
        }

        /// <summary>
        /// Updates the neighbors of all tiles within the tile map.
        /// </summary>
        public static void UpdateTileNeighbors(ref Tile[,] mapTiles, int mapWidth, int mapHeight)
        {
            Tile t = null;
            for (var x = 0; x < mapWidth; x++)
            {
                for (var y = 0; y < mapHeight; y++)
                {
                    t = mapTiles[x, y];

                    t.top = Tile.GetTop(t, ref mapTiles, mapHeight);
                    t.bottom = Tile.GetBottom(t, ref mapTiles, mapHeight);
                    t.left = Tile.GetLeft(t, ref mapTiles, mapWidth);
                    t.right = Tile.GetRight(t, ref mapTiles, mapWidth);

                    // update the bitmask for the tile to determine collidability.
                    t.UpdateBitmask();
                }
            }
        }

        /// <summary>
        /// Updates the bitmasks for all tiles within the tile map.
        /// </summary>
        [Obsolete("UpdateTileBitmasks is now called within UpdateTileNeighbors() to prevent n*n for-loop being called again just for setting a bitmask. Chances are the neighbor update is more important and can handle the bitmask update call, but if desired, leaving this method for manual use.")]
        public static void UpdateTileBitmasks(ref Tile[,] mapTiles, int mapWidth, int mapHeight)
        {
            for (var x = 0; x < mapWidth; x++)
            {
                for (var y = 0; y < mapHeight; y++)
                {
                    mapTiles[x, y].UpdateBitmask();
                }
            }
        }
    }
}