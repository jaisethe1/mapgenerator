﻿using System.Collections.Generic;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// The type a given TileGroup is.
    /// </summary>
    public enum TileGroupType
    {
        Water,  // tiles are water-based.
        Land    // tiles are land-based.
    }

    /// <summary>
    /// The group of tiles all of the same type that are close by each
    /// other in position. A continuous body of tiles.
    /// </summary>
    public class TileGroup
    {
        /// <summary>
        /// The type of tiles the group consists of.
        /// </summary>
        public TileGroupType type;

        /// <summary>
        /// The tiles of the group.
        /// </summary>
        public List<Tile> tiles;

        /// <summary>
        /// Default CTOR.
        /// </summary>
        public TileGroup()
        {
            tiles = new List<Tile>();
        }
    }

}