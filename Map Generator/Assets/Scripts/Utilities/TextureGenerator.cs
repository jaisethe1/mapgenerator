﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Generates textures for various Tile and Map types.
    /// </summary>
    public static class TextureGenerator
    {
        public const float BW_LERP = .6f;
        /// <summary>
        /// Creates a colored texture based on the Terrain Type data already assigned to the tile.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="tiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateTerrainTypeTexture(int width, int height, ref Tile[,] tiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    pixels[x + y * width] = tiles[x, y].terrainTypeData.color;

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (tiles[x, y].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a colored texture based on the Terrain Type data already assigned to the tile.
        /// This uses a defined chunk size and index to create a sub-texture from a map of tiles.
        /// </summary>
        /// <param name="chunkIndex">the index to use for a piece of the texture, representative of a chunk of terrain.</param>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="tiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateTerrainTypeTexture(Vector2 chunkIndex, int width, int height, ref Tile[,] tiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    int tileX = x + ((int)chunkIndex.x * width);
                    int tileY = y + ((int)chunkIndex.y * height);

                    // modify the reference indices to be one left (x) and/or one down (y)
                    // so that the textures align properly for all terrain chunks
                    // after the zero-th row/column. I.E. index x or y = 0
                    if ((int)chunkIndex.x > 0)
                    {
                        tileX = Mathf.Clamp(tileX - 1, 0, int.MaxValue);
                    }
                    if ((int)chunkIndex.y > 0)
                    {
                        tileX = Mathf.Clamp(tileX - 1, 0, int.MaxValue);
                    }

                    pixels[x + y * width] = tiles[tileX, tileY].terrainTypeData.color;

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (tiles[tileX, tileY].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Trilinear;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a black and white texture based on height data already assigned to the tiles.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="tiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateHeightBlackWhiteTexture(int width, int height, ref Tile[,] tiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    float value = tiles[x, y].heightValue;

                    //Set color range, 0 = black, 1 = white
                    pixels[x + y * width] = Color.Lerp(Color.black, Color.white, value);

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (tiles[x, y].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a colored texture based on water and land groups.
        /// It colors land, ocean, and lake tiles.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="waterGroups">reference to list of water tile groups</param>
        /// <param name="landGroups">reference to list of land tile groups</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateLandMassTexture(int width, int height, ref List<TileGroup> waterGroups, ref List<TileGroup> landGroups, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            float area = width * height;
            area *= .15f; // if greater than 15% of area, then consider it ocean > lake

            Color tileColor;
            Tile t;
            for (int i = 0; i < waterGroups.Count; ++i)
            {
                tileColor = Color.blue + (Color.white * .5f); // lake

                if (waterGroups[i].tiles.Count > area)
                    tileColor = Color.blue; // ocean
                for (int iTile = 0; iTile < waterGroups[i].tiles.Count; ++iTile)
                {
                    t = waterGroups[i].tiles[iTile];
                    pixels[t.x + t.y * width] = tileColor;

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (t.bitmask != 15)
                            pixels[t.x + t.y * width] = Color.Lerp(pixels[t.x + t.y * width], Color.black, BW_LERP);
                    }
                }
            }
            for (int i = 0; i < landGroups.Count; ++i)
            {
                for (int iTile = 0; iTile < landGroups[i].tiles.Count; ++iTile)
                {
                    t = landGroups[i].tiles[iTile];
                    pixels[t.x + t.y * width] = Color.green; // land

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (t.bitmask != 15)
                            pixels[t.x + t.y * width] = Color.Lerp(pixels[t.x + t.y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a generic colored texture based on the heat index data already assigned to the tile.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="heatTiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateHeatMapTexture(int width, int height, ref Tile[,] heatTiles, ref Tile[,] heightTiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            Tile t;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    t = heatTiles[x, y];
                    pixels[x + y * width] = Color.Lerp(Color.blue, Color.red, t.heatValue);

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (heightTiles[x, y].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a colored texture based on the HeatType data already assigned to the tile.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="heatTiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateHeatTypeMapTexture(int width, int height, ref Tile[,] heatTiles, ref Tile[,] heightTiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];
            
            Tile t;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    t = heatTiles[x, y];
                    pixels[x + y * width] = t.heatTypeData.color;

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (heightTiles[x, y].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a generic colored texture based on the moisture value data already assigned to the tile.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="moistureTiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateMoistureMapTexture(int width, int height, ref Tile[,] moistureTiles, ref Tile[,] heightTiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            Tile t;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    t = moistureTiles[x, y];
                    pixels[x + y * width] = Color.Lerp(Color.blue, Color.red, t.moistureValue);

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (heightTiles[x, y].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a colored texture based on the MoistureType data already assigned to the tile.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="moistureTiles">a reference to the list of tiles</param>
        /// <param name="outline">flag used to determine if adjustments to the color of the texture
        /// should be made based on if at the edge of a terrain type. This assumes the tile's height
        /// has been set AND it's neighbors AND bitmasks are valid.</param>
        /// <returns></returns>
        public static Texture2D CreateMoistureTypeMapTexture(int width, int height, ref Tile[,] moistureTiles, ref Tile[,] heightTiles, bool outline = false)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];
            
            Tile t;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    t = moistureTiles[x, y];
                    pixels[x + y * width] = t.moistureTypeData.color;

                    //darken the color if a edge tile
                    if (outline)
                    {
                        if (heightTiles[x, y].bitmask != 15)
                            pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, BW_LERP);
                    }
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        /// <summary>
        /// Creates a colored texture based on the Terrain Type data already assigned to the tile.
        /// </summary>
        /// <param name="width">width of the tile data</param>
        /// <param name="height">height of the tile data</param>
        /// <param name="pixelColors">a reference to the list of colors for pixels</param>
        /// <returns></returns>
        public static Texture2D CreateTexture(int width, int height, ref Color[,] pixelColors)
        {
            var texture = new Texture2D(width, height);
            var pixels = new Color[width * height];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    pixels[x + y * width] = pixelColors[x, y];
                }
            }

            texture.SetPixels(pixels);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

#if UNITY_EDITOR

        /// <summary>
        /// Saves a texture as a PNG to the specified path within the main Unity dir "Assets/".
        /// </summary>
        /// <param name="texture">Texture to save out</param>
        /// <param name="fileName">The path/file name to save texture as, within the Unity subspace.</param>
        public static void SaveTextureAsPNG(Texture2D texture, string fileName)
        {
            File.WriteAllBytes(Application.dataPath + "/" + fileName, texture.EncodeToPNG());
        }

        /// <summary>
        /// Loads a Texture from within the "Assets/Resources" directory ONLY.
        /// </summary>
        /// <param name="fileName">Path/Filename of texture to load within the Resources dir, WITHOUT extension. Assumes PNG!</param>
        /// <param name="extension">Extension of texture to load, including the "." IE ".png".</param>
        /// <returns></returns>
        public static Texture2D LoadTexture(string fileName, string extension = ".png")
        {
            string filePath = Application.dataPath + "/Resources/" + fileName + extension;
            if (File.Exists(filePath))
            {
                return Resources.Load<Texture2D>(fileName);
            }

            return null;
        }
    }

#endif
}