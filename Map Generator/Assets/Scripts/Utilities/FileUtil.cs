﻿using System.IO;

/// <summary>
/// A utility class specific to dealing with file I/O.
/// </summary>
public static class FileUtil 
{
    /// <summary>
    /// Writes a string of contents to a file of a given name within the Assets/Resources dir.
    /// </summary>
    /// <param name="contents">The string to write out.</param>
    /// <param name="filename">The name of the file to write to / create. No extension needed, assumes .txt</param>
    public static void WriteToTextFile(string contents, string filename)
    {
        string path = string.Format("Assets/Resources/{0}.txt", filename);

        StreamWriter writer = new StreamWriter(path);
        writer.WriteLine(contents);
        writer.Close();
    }
}
