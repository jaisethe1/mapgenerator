﻿using UnityEngine;
using UnityEditor;
using System.IO;

public static class ScriptableObjectUtil
{
    /// <summary>
    //	This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    /// <param name="pathFromAssetsDir">The path from Assets/ dir. WITH trailing slash.</param>
    /// <param name="filename">The filename of the asset with no extension.</param>
    public static T CreateAsset<T>(string pathFromAssetsDir = "", string filename = "") where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = string.Format("Assets/{0}", pathFromAssetsDir);
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = "";

        if (filename == string.Empty)
            assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");
        else
            assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + filename + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;

        return asset;
    }
}