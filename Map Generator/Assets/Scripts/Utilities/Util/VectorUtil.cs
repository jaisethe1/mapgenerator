﻿using System.Text;
using UnityEngine;

public static partial class Util
{
    /// <summary>
    /// Returns a string representation of a vector displaying a desired number of decimals; defaulting at 5.
    /// </summary>
    /// <param name="v">The vector to print</param>
    /// <param name="numDecimals">The number of decimals to print out.</param>
    /// <returns>The string representation of the vector.</returns>
    /// <remarks>
    /// Unity does not provide ability to debug/print vectors explicitly.
    /// </remarks>
    public static string ToString(this Vector2 v, int numOfDecimals = 5)
    {
        return v.ToString( string.Format("G{0}", numOfDecimals) );
    }

    /// <summary>
    /// Returns a string representation of a vector displaying a desired number of decimals; defaulting at 5.
    /// </summary>
    /// <param name="v">The vector to print</param>
    /// <param name="numDecimals">The number of decimals to print out.</param>
    /// <returns>The string representation of the vector.</returns>
    /// <remarks>
    /// Unity does not provide ability to debug/print vectors explicitly.
    /// </remarks>
    public static string ToString(this Vector3 v, int numOfDecimals = 5)
    {
        return v.ToString(string.Format("G{0}", numOfDecimals));
    }

    /// <summary>
    /// Returns a string representation of a vector displaying a desired number of decimals; defaulting at 5.
    /// </summary>
    /// <param name="v">The vector to print</param>
    /// <param name="numDecimals">The number of decimals to print out.</param>
    /// <returns>The string representation of the vector.</returns>
    /// <remarks>
    /// Unity does not provide ability to debug/print vectors explicitly.
    /// </remarks>
    public static string ToString(this Vector4 v, int numOfDecimals = 5)
    {
        return v.ToString(string.Format("G{0}", numOfDecimals));
    }

    /// <summary>
    /// Returns a string representation of a vector array displaying a desired number of decimals; defaulting at 5. 
    /// Each vector is on a new-line.
    /// </summary>
    /// <param name="vs">The vector array to print</param>
    /// <param name="numOfDecimals">The number of decimals to print out.</param>
    /// <returns>The string representation of the vector array with each vector on a new-line.</returns>
    public static string ToString(this Vector2[] vs, int numOfDecimals = 5)
    {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < vs.Length; ++i)
        {
            sb.AppendLine(vs[i].ToString(numOfDecimals));
        }

        return sb.ToString();
    }

    /// <summary>
    /// Returns a string representation of a vector array displaying a desired number of decimals; defaulting at 5. 
    /// Each vector is on a new-line.
    /// </summary>
    /// <param name="vs">The vector array to print</param>
    /// <param name="numOfDecimals">The number of decimals to print out.</param>
    /// <returns>The string representation of the vector array with each vector on a new-line.</returns>
    public static string ToString(this Vector3[] vs, int numOfDecimals = 5)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < vs.Length; ++i)
        {
            sb.AppendLine(vs[i].ToString(numOfDecimals));
        }

        return sb.ToString();
    }

    /// <summary>
    /// Returns a string representation of a vector array displaying a desired number of decimals; defaulting at 5. 
    /// Each vector is on a new-line.
    /// </summary>
    /// <param name="vs">The vector array to print</param>
    /// <param name="numOfDecimals">The number of decimals to print out.</param>
    /// <returns>The string representation of the vector array with each vector on a new-line.</returns>
    public static string ToString(this Vector4[] vs, int numOfDecimals = 5)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < vs.Length; ++i)
        {
            sb.AppendLine(vs[i].ToString(numOfDecimals));
        }

        return sb.ToString();
    }
}
