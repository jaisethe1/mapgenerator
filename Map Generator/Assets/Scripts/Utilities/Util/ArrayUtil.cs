﻿using System;

public static partial class Util
{
    /// <summary>
    /// Splices a section of an array into a sub array.
    /// </summary>
    /// <typeparam name="T">The array type being spliced.</typeparam>
    /// <param name="data">The source array to splice.</param>
    /// <param name="startIndex">The index to start.</param>
    /// <param name="qty">The number of elements to include from the start index.</param>
    /// <returns></returns>
    public static T[] SubArray<T>(this T[] data, int startIndex, int qty)
    {
        T[] result = new T[qty];
        Array.Copy(data, startIndex, result, 0, qty);
        return result;
    }
}
