﻿using UnityEngine;

public static partial class Util
{
    /// <summary>
    /// Destroys all of the children of the given transform.
    /// </summary>
    /// <param name="transform">The transform parent to delete children from.</param>
    public static void DestroyChildren(this Transform transform)
    {
        int i = 0;
        GameObject[] allChildren = new GameObject[transform.childCount];
        foreach (Transform child in transform)
        {
            allChildren[i] = child.gameObject;
            i += 1;
        }

        foreach (GameObject child in allChildren)
        {
            if (Application.isPlaying)
                GameObject.Destroy(child.gameObject);
            else
                GameObject.DestroyImmediate(child.gameObject);
        }
    }
}
