﻿using UnityEngine;

public class SelectableLabelAttribute : PropertyAttribute
{
    public readonly string title;


    public SelectableLabelAttribute()
    {
        this.title = "";
    }

    public SelectableLabelAttribute(string _title)
    {
        this.title = _title;
    }
}