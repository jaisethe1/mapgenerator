﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Defines a configuration for moisture map thresholds.
    /// </summary>
    [Serializable]
    public class MoistureTypeConfiguration : BaseTypeConfiguration
    {
        /// <summary>
        /// Ordering of moisture types.
        /// </summary>
        public MoistureTypePair[] moistureTypeOrdering;

#if UNITY_EDITOR

        /// <summary>
        /// Finds and loads the first config file we can find.
        /// </summary>
        /// <returns></returns>
        public static MoistureTypeConfiguration DefaultConfig()
        {
            string[] terrainConfigs = AssetDatabase.FindAssets("t:MoistureTypeConfiguration");
            if (terrainConfigs.Length > 0)
            {
                string filePath;
                MoistureTypeConfiguration config;
                for (int iPath = 0; iPath < terrainConfigs.Length; ++iPath)
                {
                    filePath = AssetDatabase.GUIDToAssetPath(terrainConfigs[iPath]);
                    config = AssetDatabase.LoadAssetAtPath<MoistureTypeConfiguration>(filePath);
                    if (config != null && config.isDefaultConfig)
                        return config;
                }
            }
            Debug.LogWarning("No default MoistureTypeConfiguration to load. You should create one.");
            return null;
        }

#endif

    }

}