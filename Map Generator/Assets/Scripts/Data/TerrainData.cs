﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Noodle.MapGenerator
{

    /// <summary>
    /// The data class that houses all of the information about a piece of terrain.
    /// Height, heat, and moisture maps along with seeding information
    /// and the details of every tile within the map.
    /// </summary>
    [Serializable]
    public class TerrainData
    {
        [Header("Terrain Parameters")]

        /// <summary>
        /// The size of the map. Height and Width are equal.
        /// </summary>
        [SerializeField]
        [ReadOnly]
        public int mapSize;

        /// <summary>
        /// True to use a random seed each new generation.
        /// </summary>
        public bool useRandomSeed = true;

        /// <summary>
        /// The current seed used to generate the noise data.
        /// </summary>
        public int seed;

        /// <summary>
        /// True to draw an outline around the terrain types
        /// on the texture.
        /// </summary>
        public bool outlineTerrainTypes = false;

        [Header("Data Maps")]

        /// <summary>
        /// Flag to ignore generating and using heat map data.
        /// </summary>
        public bool ignoreHeatMap = false;

        /// <summary>
        /// Flag to ignore generating and using moisture map data.
        /// </summary>
        public bool ignoreMoistureMap = false;

        /// <summary>
        /// The Height Map data object.
        /// </summary>   
        public HeightMap heightMap = null;

        /// <summary>
        /// The Heat Map data object.
        /// </summary>
        public HeatMap heatMap = null;

        /// <summary>
        /// The Moisture Map data object.
        /// </summary>
        public MoistureMap moistureMap = null;

        [Header("Map Configurations")]
        /// <summary>
        /// The config that contains data about terrain types
        /// and how they rank in terms of height.
        /// </summary>
        public TerrainTypeConfiguration terrainTypesConfig = null;

        /// <summary>
        /// The config that contains data about heat types
        /// and how they rank in terms of their heat.
        /// </summary>
        public HeatTypeConfiguration heatTypesConfig = null;

        /// <summary>
        /// The config that contains data about moisture types
        /// and how they rank in terms of their moisture.
        /// </summary>
        public MoistureTypeConfiguration moistureTypesConfig = null;

        [Header("In Game Tiles")]

        /// <summary>
        /// The multiplier amount of the height when creating the mesh.
        /// A higher value = higher highs.
        /// </summary>
        public float heightMultiplier = 1f;

        /// <summary>
        /// The interp curve applied to height when calculating mesh height values.
        /// This is/can be used for keeping certain height values consistent within a range.
        /// For example:
        ///     If water exists from 0.0 - 0.4, then the curve can be keys such that all the water
        ///     is leveled at the same value, say 0.0, so there isn't hills/bumps within the water.
        /// </summary>
        public AnimationCurve heightCurve;

        /// <summary>
        /// The minimum height the terrain can have.
        /// </summary>
        public float MinHeight
        {
            get
            {
                return heightMultiplier * heightCurve.Evaluate(0);
            }
        }

        /// <summary>
        /// The maximum height the terrain can have.
        /// </summary>
        public float MaxHeight
        {
            get
            {
                return heightMultiplier * heightCurve.Evaluate(1);
            }
        }

        /// <summary>
        /// The level of detail of creation for the map mesh.
        /// 
        /// 1 = no simplification, a vert -1 for map size.
        /// 
        /// 2, 4, 6, 8, 10, 12 = the skipping of each x-vert for simplification.
        /// </summary>
        /// In unity editor, 0-6 as we can multiply the value by 2. This way prevents any out-of-range numbers in-editor.
        [Range(0, 6)]
        public int levelOfDetail = 0;

        /// <summary>
        /// A flag indicating to save/load textures from disk or to generate each time.
        /// </summary>
        public bool saveLoadTextures = false;

        /// <summary>
        /// The flag indicating if all texture forms (height, heat, moisture) and all permutations of them 
        /// should be generated, or simply the colored height map.
        /// </summary>
        public bool createAllTextureForms = false;

        /// <summary>
        /// The tile data for all tiles in the map.
        /// </summary>
        public Tile[,] mapTiles;

        /// <summary>
        /// The groups of water tiles.
        /// </summary>
        public List<TileGroup> waterTileGroups = new List<TileGroup>();

        /// <summary>
        /// The groups of land tiles.
        /// </summary>
        public List<TileGroup> landTileGroups = new List<TileGroup>();

        // values set and checked to see if the generating data 'is dirty' so we can regenerate the data/mesh.
        private int oldSeed = 0;
        private bool oldIgnoreHeatMap = false;
        private bool oldIgnoreMoistureMap = false;

        [Header("Debug")]
        [SerializeField]
        private bool printStopwatchTimings = false;

        /// <summary>
        /// Have to use System random gen instead of Unity's so that
        /// way we can thread data generation. Unity's random gen
        /// has to be called on the main thread.
        /// </summary>
        private static System.Random randomGen = new System.Random();

        /// <summary>
        /// Creates a base Terrain Data of a given map size.
        /// </summary>
        /// <param name="mapSize">The height/width of the map.</param>
        public TerrainData(int mapSize)
        {
            this.mapSize = mapSize;

            heightMap = new HeightMap();
            heatMap = new HeatMap();
            moistureMap = new MoistureMap();

            mapTiles = new Tile[mapSize, mapSize];
        }

        /// <summary>
        /// Creates a Terrain Data of a given size with a specific seed value.
        /// </summary>
        /// <param name="mapSize">The height/width of the map.</param>
        /// <param name="seed">The seed value to generate initial map tile values.</param>
        public TerrainData(int mapSize, int seed) : this(mapSize)
        {
            this.seed = seed;
        }

        /// <summary>
        /// Creates a copy of the given terrain data.
        /// </summary>
        /// <param name="copy">The terrain data to copy</param>
        /// <param name="produceATrueCopy">Flag indicating to produce a true copy of the given terrain data or not.
        /// True will use the same seed and will continue ignoring heat/moisture maps.
        /// False will create a new random seed and will force update heat/moisture maps.</param>
        /// <param name="terrainTypeConfig">The terrain type configuration.</param>
        /// <param name="numTerrainChunks">The number of terrain chunks in one direction. I.E. 3 = 3x3 = 9 total chunks</param>
        public TerrainData(TerrainData copy, bool produceATrueCopy, TerrainTypeConfiguration terrainTypeConfig, int numTerrainChunks = 1)
        {
            // the map size will need to encompass data for all terrain chunks
            mapSize = copy.mapSize * numTerrainChunks;
            useRandomSeed = produceATrueCopy ? copy.useRandomSeed : true;
            seed = copy.seed;
            outlineTerrainTypes = copy.outlineTerrainTypes;

            ignoreHeatMap = produceATrueCopy ? copy.ignoreHeatMap : true;
            ignoreMoistureMap = produceATrueCopy ? copy.ignoreMoistureMap : true;

            heightMap = copy.heightMap;
            heatMap = copy.heatMap;
            moistureMap = copy.moistureMap;

            terrainTypesConfig = terrainTypeConfig;

            heatTypesConfig = copy.heatTypesConfig;
            moistureTypesConfig = copy.moistureTypesConfig;

            heightMultiplier = copy.heightMultiplier;
            heightCurve = copy.heightCurve;
            levelOfDetail = copy.levelOfDetail;

            saveLoadTextures = copy.saveLoadTextures;
            createAllTextureForms = copy.createAllTextureForms;

            mapTiles = new Tile[mapSize, mapSize];

            waterTileGroups = copy.waterTileGroups;
            landTileGroups = copy.landTileGroups;

            oldSeed = copy.oldSeed;
            oldIgnoreHeatMap = copy.oldIgnoreHeatMap;
            oldIgnoreMoistureMap = copy.oldIgnoreMoistureMap;

            // adjust the frequency based on the number of terrain chunks.
            // this is because we want more terrain features for a larger map.
            //TODO: this is a simple formula which could probably use some more tweaking, will see.
            int adjustedFrequencyBasedOnSize = (1 + (numTerrainChunks * numTerrainChunks)) / 2; 
            heightMap.terrainFrequency = adjustedFrequencyBasedOnSize;
            heatMap.heatFrequency = adjustedFrequencyBasedOnSize;
            moistureMap.moistureFrequency= adjustedFrequencyBasedOnSize;

            //TODO: Need to see if we really need to modify terrain height thresholds.
            /*if (numTerrainChunks > 1)
            {
                float percentage = .15f;// numTerrainChunks * .15f;
                for (int i = this.terrainTypesConfig.terrainTypeOrdering.Length-1; i > 0; --i)
                {

                    percentage = percentage - (percentage * .15f);

                    TerrainTypePair currentPair = this.terrainTypesConfig.terrainTypeOrdering[i];
                    TerrainTypePair nextPair = (i == 0) ? null : this.terrainTypesConfig.terrainTypeOrdering[i-1];

                    float curThreshold = currentPair.terrainHeightThreshold;
                    float nextThreshold = (i == 0) ? 0 : nextPair.terrainHeightThreshold;
                    float diff = curThreshold - nextThreshold;

                    curThreshold = curThreshold + (diff * percentage);// Mathf.Clamp(curThreshold + (diff * percentage), 0, 1);
                    this.terrainTypesConfig.terrainTypeOrdering[i].terrainHeightThreshold = curThreshold;
                    float maxThreshold = this.terrainTypesConfig.terrainTypeOrdering[this.terrainTypesConfig.terrainTypeOrdering.Length - 1].terrainHeightThreshold;
                    this.terrainTypesConfig.terrainTypeOrdering[i].terrainHeightThreshold = this.terrainTypesConfig.terrainTypeOrdering[i].terrainHeightThreshold / maxThreshold;
                }
            }*/

            //printStopwatchTimings = true;
        }

        /// <summary>
        /// A full update of the current map with current parameters.
        /// </summary>
        public void UpdateMaps(bool forceUpdate = false)
        {
            // generate a new random seed, if flagged
            if (useRandomSeed)
                seed = randomGen.Next(0, int.MaxValue);

            // determine if the map generation data fields are dirty based on previous values
            bool isDirty = false;
            if (oldSeed != seed || oldIgnoreHeatMap != ignoreHeatMap || oldIgnoreMoistureMap != ignoreMoistureMap || mapTiles == null)
            {
                oldSeed = seed;
                oldIgnoreHeatMap = ignoreHeatMap;
                oldIgnoreMoistureMap = ignoreMoistureMap;
                isDirty = true;
            }

            // no point in re-generating/calculating map data if it is not dirty or isn't being forced to.
            if (!isDirty && !forceUpdate)
                return;

            // time each step to figure out bottlenecks
            Stopwatch st = new Stopwatch();

            // update the maps
            {
                st.Start();
                {
                    heightMap.UpdateMap(mapSize, mapSize, seed);
                }
                st.Stop();
                if (printStopwatchTimings)
                    Debug.Log(string.Format("heightMap.UpdateMap took {0} ms to complete", st.ElapsedMilliseconds));

                st.Reset();
                st.Start();
                {
                    if (!ignoreHeatMap)
                        heatMap.UpdateMap(mapSize, mapSize, seed);
                }
                st.Stop();
                if (printStopwatchTimings)
                    Debug.Log(string.Format("heatMap.UpdateMap took {0} ms to complete", st.ElapsedMilliseconds));

                st.Reset();
                st.Start();
                {
                    if (!ignoreMoistureMap)
                        moistureMap.UpdateMap(mapSize, mapSize, seed);
                }
                st.Stop();
                if (printStopwatchTimings)
                    Debug.Log(string.Format("moistureMap.UpdateMap took {0} ms to complete", st.ElapsedMilliseconds));
            }

            if (waterTileGroups == null)
                waterTileGroups = new List<TileGroup>();
            if (landTileGroups == null)
                landTileGroups = new List<TileGroup>();
        }

        /// <summary>
        /// Update all of the tiles with the current maps.
        /// </summary>
        public void UpdateTileData()
        {
            Stopwatch st = new Stopwatch();

            st.Start();
            {
                // load tiles with all map datas
                HeatMap loadTilesHeatMap = (ignoreHeatMap) ? null : heatMap;
                MoistureMap loadTilesMoistureMap = (ignoreMoistureMap) ? null : moistureMap;
                LoadTilesWithMapData(mapSize, mapSize, heightMap, loadTilesHeatMap, loadTilesMoistureMap, terrainTypesConfig, heatTypesConfig, moistureTypesConfig, out mapTiles);
            }
            st.Stop();
            if (printStopwatchTimings)
                Debug.Log(string.Format("LoadTilesWithMapData took {0} ms to complete", st.ElapsedMilliseconds));

            st.Reset();
            st.Start();
            {
                // find tile neighbors and set bit masks based on neighbor terrain types.
                Tile.UpdateTileNeighbors(ref mapTiles, mapSize, mapSize);
            }
            st.Stop();
            if (printStopwatchTimings)
                Debug.Log(string.Format("UpdateTileNeighbors took {0} ms to complete", st.ElapsedMilliseconds));

            st.Reset();
            st.Start();
            {
                // determine land/water groups
                FloodFill();
            }
            st.Stop();
            if (printStopwatchTimings)
                Debug.Log(string.Format("FloodFill took {0} ms to complete", st.ElapsedMilliseconds));
        }

        /// <summary>
        /// Creates a texture for a chunk of the height map; colored based on terrain types.
        /// </summary>
        /// <param name="terrainChunkIndex">The 0-indexed chunk index.</param>
        /// <param name="chunkSize">The size of the chunk.</param>
        /// <param name="saveLoadTextures">True to load a saved texture, false to create a new one.</param>
        /// <returns>The texture for a given chunk index.</returns>
        public Texture2D CreateColoredHeightMapTextureForChunk(Vector2 terrainChunkIndex, int chunkSize, bool saveLoadTextures = false)
        {
            if (saveLoadTextures)
                return TextureGenerator.LoadTexture("Textures/heightMapRenderer");
            else
                return TextureGenerator.CreateTerrainTypeTexture(terrainChunkIndex, chunkSize, chunkSize, ref mapTiles, outlineTerrainTypes);
        }

        /// <summary>
        /// Loads an array of tiles with information from terrain and height data.
        /// </summary>
        /// <param name="mapWidth">The width of the terrain.</param>
        /// <param name="mapHeight">The height of the terrain.</param>
        /// <param name="heightMap">The map object which defines the height data for the terrain.</param>
        /// <param name="heatMap">The map object which defines the heat data for the terrain.</param>
        /// <param name="moistureMap">The map object which defines the moisture data for the terrain.</param>
        /// <param name="terrainTypesConfig">The config asset defining the types of terrain available.</param>
        /// <param name="heatTypeConfig">The config asset defining the types of heat available.</param>
        /// <param name="moistureTypeConfig">The config asset defining the types of moisture available.</param>
        /// <param name="tiles">The 2D array of tiles which define features/details for each tile of the map.</param>
        public static void LoadTilesWithMapData(int mapWidth, int mapHeight, HeightMap heightMap, HeatMap heatMap, MoistureMap moistureMap,
            TerrainTypeConfiguration terrainTypesConfig, HeatTypeConfiguration heatTypeConfig, MoistureTypeConfiguration moistureTypeConfig,
            out Tile[,] tiles)
        {
            tiles = new Tile[mapWidth, mapHeight];

            for (var x = 0; x < mapWidth; x++)
            {
                for (var y = 0; y < mapHeight; y++)
                {
                    Tile t = new Tile();
                    t.x = x;
                    t.y = y;

                    // find and set height value and determine terrain types, neighbors, etc
                    if (heightMap != null)
                    {
                        HeightMap.UpdateTileWithHeight(ref t, heightMap, terrainTypesConfig);
                    }

                    // find and set heat value and
                    // adjust heat map based on height - higher == colder
                    if (heatMap != null)
                    {
                        HeatMap.UpdateTileWithHeat(ref t, heatMap, heatTypeConfig);
                    }

                    // find and set moisture value and
                    // adjust moisture map based on Height - lower == wetter
                    if (moistureMap != null)
                    {
                        MoistureMap.UpdateTileWithMoisture(ref t, moistureMap, moistureTypeConfig);
                    }

                    tiles[x, y] = t;
                }
            }
        }

        /// <summary>
        /// Flood fill determines where things such as land masses, lakes,
        /// oceans are in the map; and how large they are.
        /// </summary>
        /// <remarks>
        /// Since the map could potentially be very large, we cannot use a 
        /// recursive flood fill, as it would easily produce stack overflow 
        /// exceptions. Instead, we will need to use a non-recursive approach 
        /// to solve this problem.
        /// </remarks>
        private void FloodFill()
        {
            // Use a stack instead of recursion
            Stack<Tile> stack = new Stack<Tile>();

            for (int x = 0; x < mapSize; x++)
            {
                for (int y = 0; y < mapSize; y++)
                {

                    Tile t = mapTiles[x, y];

                    //Tile already flood filled, skip
                    if (t.floodFilled) continue;

                    // Land
                    if (t.collidable)
                    {
                        TileGroup group = new TileGroup();
                        group.type = TileGroupType.Land;
                        stack.Push(t);

                        while (stack.Count > 0)
                        {
                            FloodFillHelper(stack.Pop(), ref group, ref stack);
                        }

                        if (group.tiles.Count > 0)
                            landTileGroups.Add(group);
                    }
                    // Water
                    else
                    {
                        TileGroup group = new TileGroup();
                        group.type = TileGroupType.Water;
                        stack.Push(t);

                        while (stack.Count > 0)
                        {
                            FloodFillHelper(stack.Pop(), ref group, ref stack);
                        }

                        if (group.tiles.Count > 0)
                            waterTileGroups.Add(group);
                    }
                }
            }
        }

        /// <summary>
        /// Helper function for flood fill that actually validates 
        /// and adds the tile to the given group. Then flood fills the
        /// tile's neighbors.
        /// </summary>
        /// <param name="tile"></param>
        /// <param name="tiles"></param>
        /// <param name="stack"></param>
        private void FloodFillHelper(Tile tile, ref TileGroup tiles, ref Stack<Tile> stack)
        {
            // validate
            if (tile.floodFilled)
                return;
            if (tiles.type == TileGroupType.Land && !tile.collidable)
                return;
            if (tiles.type == TileGroupType.Water && tile.collidable)
                return;

            // add to TileGroup
            tiles.tiles.Add(tile);
            tile.floodFilled = true;

            // floodfill into neighbors
            Tile t = Tile.GetTop(tile, ref mapTiles, mapSize);
            if (!t.floodFilled && tile.collidable == t.collidable)
                stack.Push(t);

            t = Tile.GetBottom(tile, ref mapTiles, mapSize);
            if (!t.floodFilled && tile.collidable == t.collidable)
                stack.Push(t);

            t = Tile.GetLeft(tile, ref mapTiles, mapSize);
            if (!t.floodFilled && tile.collidable == t.collidable)
                stack.Push(t);

            t = Tile.GetRight(tile, ref mapTiles, mapSize);
            if (!t.floodFilled && tile.collidable == t.collidable)
                stack.Push(t);
        }
    }

}