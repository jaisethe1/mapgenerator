﻿
using AccidentalNoise;
using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Holds the shared data for map types such as Heat, Height, Moisture, etc.
    /// </summary>
    [Serializable]
    public class BaseMap
    {
        /// <summary>
        /// Width of the map.
        /// </summary>
        [HideInInspector()]
        public int width = 512;

        /// <summary>
        /// Height of the map.
        /// </summary>
        [HideInInspector()]
        public int height = 512;

        /// <summary>
        /// The map data (fully consolidated).
        /// </summary>
        [HideInInspector()]
        public MapData mapData;

        /// <summary>
        /// The type of wrap used when generating the map.
        /// </summary>
        public WrapMode wrapMode = WrapMode.NoWrap;

        [Header("Masking Texture")]
        /// <summary>
        /// A mask to combine with a map to a more desirable form but with some randomness.
        /// Assumes a texture same size as the map, or it will be scaled to match...whatever consequences this may cause.
        /// </summary>
        public Texture2D maskTexture;

        /// <summary>
        /// Weight of mask on random data
        /// </summary>
        [Range(0, 1)]
        public float maskWeight = .5f;

        /// <summary>
        /// True to use the referenced mask, false to generate without mask.
        /// Utility to prevent having to re-set the mask if testing.
        /// </summary>
        public bool useMask = false;

        /// <summary>
        /// True to invert the mask texture values (utility to prevent having make inverse textures)
        /// </summary>
        public bool invertMask = false;

        public const float TWO_PI = 2 * Mathf.PI;

        public virtual void UpdateMap(int width, int height, int seed)
        {
            this.width = width;
            this.height = height;
            mapData = new MapData(width, height);

            // ensure masking texture is same size as map, if using a mask texture
            if (useMask && maskTexture != null)
            {
                if(maskTexture.width != width || maskTexture.height != height)
                {
                    var newTex = GameObject.Instantiate(maskTexture);

                    TextureScale.Point(newTex, width, height);

                    maskTexture = newTex;
                }
            }
        }

        /// <summary>
        /// Assumes an already non-null mask texture!
        /// Modifies given value by masking texture at the same position.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="valueToMod"></param>
        /// <returns></returns>
        protected float ModifyValueByMask(int x, int y, float valueToMod)
        {
            float maskValue = maskTexture.GetPixel(x, y).grayscale;

            if(invertMask)
            {
                maskValue = 1f - maskValue;
            }

            valueToMod += Mathf.Lerp(valueToMod, maskValue, maskWeight);

            return valueToMod;
        }

        /// <summary>
        /// Extracts map data from the noise generated.
        /// This uses the data as-is with no wrapping or other filtering.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="mapData"></param>
        protected void ExtractDataFromNoiseNoWrap(ImplicitModuleBase module, ref MapData mapData, bool forceMaskOff = false)
        {
            // loop through each x,y point - get height value
            float x1, y1, mapValue = 0;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    // sample the noise at smaller intervals
                    x1 = x / (float)width;
                    y1 = y / (float)height;

                    mapValue = (float)module.Get(x1, y1);

                    // alter by mask
                    if (!forceMaskOff && useMask && maskTexture != null)
                    {
                        mapValue = ModifyValueByMask(x, y, mapValue);
                    }

                    // keep track of the max and min values found
                    if (mapValue > mapData.max) mapData.max = mapValue;
                    if (mapValue < mapData.min) mapData.min = mapValue;

                    mapData.data[x, y] = mapValue;
                }
            }
        }

        /// <summary>
        /// Extracts map data from the noise generated.
        /// This will wrap the height map in the X-axis.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="mapData"></param>
        protected void ExtractDataFromNoiseWrapX(ImplicitModuleBase module, ref MapData mapData, bool forceMaskOff = false)
        {
            // noise range
            float x1 = 0, x2 = 1;
            float dx = x2 - x1;

            // loop through each x,y point - get mapped value
            float s, t = 0f;
            float nx, ny, nz = 0f;
            float mappedValue = 0f;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    // sample the noise at smaller intervals
                    s = x / (float)width;
                    t = y / (float)height;

                    // calculate 3D coord
                    nx = x1 + Mathf.Cos(s * TWO_PI) * dx / (TWO_PI);
                    ny = x1 + Mathf.Sin(s * TWO_PI) * dx / (TWO_PI);
                    nz = t;

                    mappedValue = (float)module.Get(nx, ny, nz);

                    // alter by mask
                    if (!forceMaskOff && useMask && maskTexture != null)
                    {
                        mappedValue = ModifyValueByMask(x, y, mappedValue);
                    }

                    // keep track of min/max values
                    if (mappedValue > mapData.max)
                        mapData.max = mappedValue;
                    if (mappedValue < mapData.min)
                        mapData.min = mappedValue;

                    mapData.data[x, y] = mappedValue;
                }
            }
        }

        /// <summary>
        /// Extracts height map data from the noise generated.
        /// This will wrap the height map in the X & Y axes.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="mapData"></param>
        protected void ExtractDataFromNoiseWrapXY(ImplicitModuleBase module, ref MapData mapData, bool forceMaskOff = false)
        {
            // noise range
            float x1 = 0, x2 = 2;
            float y1 = 0, y2 = 2;
            float dx = x2 - x1;
            float dy = y2 - y1;

            // loop through each x,y point - get mapped value
            float s, t = 0f;
            float nx, ny, nz, nw = 0f;
            float mappedValue = 0f;
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    // sample the noise at smaller intervals
                    s = x / (float)width;
                    t = y / (float)height;

                    // calculate 4D coord
                    nx = x1 + Mathf.Cos(s * TWO_PI) * dx / (TWO_PI);
                    ny = y1 + Mathf.Cos(t * TWO_PI) * dy / (TWO_PI);
                    nz = x1 + Mathf.Sin(s * TWO_PI) * dx / (TWO_PI);
                    nw = y1 + Mathf.Sin(t * TWO_PI) * dy / (TWO_PI);

                    mappedValue = (float)module.Get(nx, ny, nz, nw);
                    
                    // alter by mask
                    if (!forceMaskOff && useMask && maskTexture != null)
                    {
                        mappedValue = ModifyValueByMask(x, y, mappedValue);
                    }

                    // keep track of min/max values
                    if (mappedValue > mapData.max)
                        mapData.max = mappedValue;
                    if (mappedValue < mapData.min)
                        mapData.min = mappedValue;

                    mapData.data[x, y] = mappedValue;
                }
            }
        }
    }
}
