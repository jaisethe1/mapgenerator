﻿

using System;
using Rotorz.ReorderableList;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Noodle.MapGenerator
{
    [CustomEditor(typeof(MoistureTypeConfiguration))]
    public class MoistureTypeConfigurationInspector : Editor
    {
        MoistureTypeConfiguration conf;
        List<MoistureTypePair> _tempList = new List<MoistureTypePair>();

        /// <summary>
        /// True if the list contains any out of place indexed items.
        /// </summary>
        bool doesContainsInvalidItem = false;

        /// <summary>
        /// True if the list contains an item with the max value for height threshold (1).
        /// </summary>
        bool doesContainMaxValueItem = false;

        /// <summary>
        /// True if the list contains at least one null item.
        /// </summary>
        bool doesContainNullItem = false;

        void OnEnable()
        {
            conf = (MoistureTypeConfiguration)serializedObject.targetObject;
        }
        
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (conf.moistureTypeOrdering.Length > 0)
                _tempList = new List<MoistureTypePair>(conf.moistureTypeOrdering);
            else
                _tempList = new List<MoistureTypePair>();
            ReorderableListGUI.Title("Moisture Type Ordering (Moisture Thresholds)");

            if (doesContainsInvalidItem)
            {
                GUI.color = Color.red;
                ReorderableListGUI.Title("Contains Invalid Order - Check the thresholds!");
                GUI.color = Color.white;
            }

            doesContainMaxValueItem = false;
            doesContainNullItem = false;
            for (int index = _tempList.Count - 1; index >= 0; --index) // start in reverse as most likely to find max value in last position
            {
                if (_tempList[index] != null && _tempList[index].moistureValue == 1f)
                {
                    doesContainMaxValueItem = true;
                }
                else if (_tempList[index] == null)
                    doesContainNullItem = true;
            }
            if (doesContainNullItem)
            {
                GUI.color = Color.red;
                ReorderableListGUI.Title("The list contains a null moisture type!");
                GUI.color = Color.white;
            }
            if (!doesContainMaxValueItem)
            {
                GUI.color = Color.red;
                ReorderableListGUI.Title("A valid list must contain a tile with a threshold value of 1!");
                GUI.color = Color.white;
            }
            ReorderableListGUI.ListField(_tempList, CustomDrawItem, 16, ReorderableListFlags.ShowIndices);
            conf.moistureTypeOrdering = _tempList.ToArray();                          conf.typeName = EditorGUILayout.TextField("Type Name", conf.typeName);              EditorGUILayout.LabelField("Enum Type Path: " + conf.typeEnumPath);

            conf.isDefaultConfig = EditorGUILayout.Toggle("Is Default Config", conf.isDefaultConfig);

            serializedObject.ApplyModifiedProperties();

            if (!Application.isPlaying && conf.isDefaultConfig)
            {
                RegenerateMoistureTypeEnum();
            }
        }

        bool isInvalid;
        float tempVal = 0f;
        private MoistureTypePair CustomDrawItem(Rect position, MoistureTypePair item)
        {
            isInvalid = false;
            if (item != null)
            {
                int index = _tempList.IndexOf(item);
                // reset flag if first item in list
                if (index == 0)
                    doesContainsInvalidItem = false;

                // check for invalid indexed items that are out of place based on the moisture index
                if (index + 1 < _tempList.Count && _tempList[index + 1] != null)
                    isInvalid = _tempList[index + 1].moistureValue < item.moistureValue;
                if (index - 1 >= 0 && _tempList[index - 1] != null)
                    isInvalid = _tempList[index - 1].moistureValue > item.moistureValue;
            }

            // indicate the problem index with red
            GUI.color = isInvalid ? Color.red : Color.white;

            float width = position.width;
            position.width = width * .66666f;
            item.moistureType = (MoistureType)EditorGUI.ObjectField(position, item.moistureType, typeof(MoistureType), false);

            if (item != null)             {                 position.width = 50f;                 position.x = width;                 tempVal = item.moistureValue;                 item.moistureValue = EditorGUI.FloatField(position, item.moistureValue, EditorStyles.miniTextField);                 if (tempVal != item.moistureValue)                     EditorUtility.SetDirty(serializedObject.targetObject);             }

            GUI.color = Color.white;

            if (isInvalid)
                doesContainsInvalidItem = true;

            return item;
        }

        void RegenerateMoistureTypeEnum()
        {
            StringBuilder contents = new StringBuilder("// this files is automatically generated so please don't modify! \n\npublic enum " + conf.typeName + "s\n{\n\t");             int counter = 0;             foreach (MoistureTypePair terrain in _tempList)             {                 if (terrain != null)                     contents.Append(string.Concat(terrain.moistureType.name, " = ", ++counter, ",\n\t"));             }             contents.Append("}");              string path = Application.dataPath + conf.typeEnumPath;             if (File.Exists(path))             {                 string existingContents = File.ReadAllText(path);                 if (existingContents != contents.ToString())                 {                     StreamWriter writer = File.CreateText(path);                     using (writer)                     {                         writer.Write(contents);                         writer.Close();                     }                     AssetDatabase.ImportAsset("Assets/" + conf.typeEnumPath);                      EditorUtility.SetDirty(serializedObject.targetObject);                 }              }             else             {                 int last = path.LastIndexOf("/");                 string dir = path.Remove(last, path.Length - last);                 if (!Directory.Exists(dir))                     Directory.CreateDirectory(dir);                  StreamWriter writer = File.CreateText(path);                 using (writer)                 {                     writer.Write(contents);                     writer.Close();                 }                 AssetDatabase.ImportAsset("Assets/" + conf.typeEnumPath);             }         }
    }

}