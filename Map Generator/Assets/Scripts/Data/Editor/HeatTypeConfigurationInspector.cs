﻿

using System;
using Rotorz.ReorderableList;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Noodle.MapGenerator
{
    [CustomEditor(typeof(HeatTypeConfiguration))]
    public class HeatTypeConfigurationInspector : Editor
    {
        HeatTypeConfiguration conf;
        List<HeatTypePair> _tempList = new List<HeatTypePair>();

        /// <summary>
        /// True if the list contains any out of place indexed items.
        /// </summary>
        bool doesContainsInvalidItem = false;

        /// <summary>
        /// True if the list contains an item with the max value for height threshold (1).
        /// </summary>
        bool doesContainMaxValueItem = false;

        /// <summary>
        /// True if the list contains at least one null item.
        /// </summary>
        bool doesContainNullItem = false;

        void OnEnable()
        {
            conf = (HeatTypeConfiguration)serializedObject.targetObject;
        }
        
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (conf.heatTypeOrdering.Length > 0)
                _tempList = new List<HeatTypePair>(conf.heatTypeOrdering);
            else
                _tempList = new List<HeatTypePair>();
            ReorderableListGUI.Title("Heat Type Ordering (Heat Thresholds)");

            if (doesContainsInvalidItem)
            {
                GUI.color = Color.red;
                ReorderableListGUI.Title("Contains Invalid Order - Check the thresholds!");
                GUI.color = Color.white;
            }

            doesContainMaxValueItem = false;
            doesContainNullItem = false;
            for (int index = _tempList.Count - 1; index >= 0; --index) // start in reverse as most likely to find max value in last position
            {
                if (_tempList[index] != null && _tempList[index].heatIndex == 1f)
                {
                    doesContainMaxValueItem = true;
                }
                else if (_tempList[index] == null)
                    doesContainNullItem = true;
            }
            if (doesContainNullItem)
            {
                GUI.color = Color.red;
                ReorderableListGUI.Title("The list contains a null heat type!");
                GUI.color = Color.white;
            }
            if (!doesContainMaxValueItem)
            {
                GUI.color = Color.red;
                ReorderableListGUI.Title("A valid list must contain a tile with a threshold value of 1!");
                GUI.color = Color.white;
            }
            ReorderableListGUI.ListField(_tempList, CustomDrawItem, 16, ReorderableListFlags.ShowIndices);
            conf.heatTypeOrdering = _tempList.ToArray();                          conf.typeName = EditorGUILayout.TextField("Type Name", conf.typeName);              EditorGUILayout.LabelField("Enum Type Path: " + conf.typeEnumPath);

            conf.isDefaultConfig = EditorGUILayout.Toggle("Is Default Config", conf.isDefaultConfig);

            serializedObject.ApplyModifiedProperties();

            if (!Application.isPlaying && conf.isDefaultConfig)
            {
                RegenerateHeatTypeEnum();
            }
        }

        bool isInvalid;
        float tempVal = 0f;
        private HeatTypePair CustomDrawItem(Rect position, HeatTypePair item)
        {
            isInvalid = false;
            if (item != null)
            {
                int index = _tempList.IndexOf(item);
                // reset flag if first item in list
                if (index == 0)
                    doesContainsInvalidItem = false;

                // check for invalid indexed items that are out of place based on the heat index
                if (index + 1 < _tempList.Count && _tempList[index + 1] != null)
                    isInvalid = _tempList[index + 1].heatIndex < item.heatIndex;
                if (index - 1 >= 0 && _tempList[index - 1] != null)
                    isInvalid = _tempList[index - 1].heatIndex > item.heatIndex;
            }

            // indicate the problem index with red
            GUI.color = isInvalid ? Color.red : Color.white;

            float width = position.width;
            position.width = width * .66666f;
            item.heatType = (HeatType)EditorGUI.ObjectField(position, item.heatType, typeof(HeatType), false);

            if (item != null)             {                 position.width = 50f;                 position.x = width;                 tempVal = item.heatIndex;                 item.heatIndex = EditorGUI.FloatField(position, item.heatIndex, EditorStyles.miniTextField);                 if (tempVal != item.heatIndex)                     EditorUtility.SetDirty(serializedObject.targetObject);             }

            GUI.color = Color.white;

            if (isInvalid)
                doesContainsInvalidItem = true;

            return item;
        }

        void RegenerateHeatTypeEnum()
        {
            StringBuilder contents = new StringBuilder("// this file is automatically generated so please don't modify! \n\npublic enum " + conf.typeName + "s\n{\n\t");             int counter = 0;             foreach (HeatTypePair terrain in _tempList)             {                 if (terrain != null)                     contents.Append(string.Concat(terrain.heatType.name, " = ", ++counter, ",\n\t"));             }             contents.Append("}");              string path = Application.dataPath + conf.typeEnumPath;             if (File.Exists(path))             {                 string existingContents = File.ReadAllText(path);                 if (existingContents != contents.ToString())                 {                     StreamWriter writer = File.CreateText(path);                     using (writer)                     {                         writer.Write(contents);                         writer.Close();                     }                     AssetDatabase.ImportAsset("Assets/" + conf.typeEnumPath);                      EditorUtility.SetDirty(serializedObject.targetObject);                 }              }             else             {                 int last = path.LastIndexOf("/");                 string dir = path.Remove(last, path.Length - last);                 if (!Directory.Exists(dir))                     Directory.CreateDirectory(dir);                  StreamWriter writer = File.CreateText(path);                 using (writer)                 {                     writer.Write(contents);                     writer.Close();                 }                 AssetDatabase.ImportAsset("Assets/" + conf.typeEnumPath);             }         }
    }

}