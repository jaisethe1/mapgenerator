﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TerrainDataAsset))]
public class TerrainDataAssetInspector : Editor
{
    TerrainDataAsset terrainAsset;

    public override void OnInspectorGUI()
    {
        terrainAsset = (TerrainDataAsset)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Save Asset"))
        {
            EditorUtility.SetDirty(terrainAsset);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
