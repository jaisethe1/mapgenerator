﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEditor;
//using UnityEngine;

//namespace Noodle.MapGenerator
//{
//    /// <summary>
//    /// Custom property drawer for Height Map.
//    /// </summary>
//    //[CustomPropertyDrawer(typeof(HeightMap))]
//    public class HeightMapPropertyDrawer : PropertyDrawer
//    {
//        /// <summary>
//        /// The number of properties (EXCLUDING the texture) drawn.
//        /// </summary>
//        const int NUM_OF_PROPERTIES_IN_DRAWER = 9;

//        /// <summary>
//        /// The default height of common elements
//        /// </summary>
//        const int DEFAULT_HEIGHT = 16;

//        /// <summary>
//        /// Current top-level property being edited.
//        /// </summary>
//        SerializedProperty curPropBeingEdited;

//        /// <summary>
//        /// The current drawer's rect position.
//        /// </summary>
//        Rect drawerPosition;

//        /// <summary>
//        /// The number of items drawn this OnGUI frame.
//        /// </summary>
//        int numOfItemsDrawn = 0;

//        /// <summary>
//        /// The rect height of the texture used to visualize the height map.
//        /// </summary>
//        float texturePropertyRectHeight = 0f;
        
//        /// <summary>
//        /// Draws the top-level property.
//        /// </summary>
//        /// <param name="position"></param>
//        /// <param name="property"></param>
//        /// <param name="label"></param>
//        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//        {
            
//            numOfItemsDrawn = 0;
            
//            EditorGUI.BeginProperty(position, label, property);

//            EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label, EditorStyles.miniBoldLabel);
//            position.y += DEFAULT_HEIGHT;
//            ++EditorGUI.indentLevel;
//            {
//                curPropBeingEdited = property;
//                drawerPosition = position;

//                //DrawProperty("width", "Width");
//                //DrawProperty("height", "Height");
//                DrawProperty("fractalType", "Fractal Type");
//                DrawProperty("basisType", "Basis Type");
//                DrawProperty("interpolationType", "Interpolation Type");

//                DrawPropertyRange("terrainOctaves", "Terrain Octaves", 1, 10);      // these ranges just seemed to work the best.
//                DrawPropertyRange("terrainFrequency", "Terrain Frequency", 1f, 10f);

//                DrawProperty("useRandomSeed", "Use Random Seed");
//                DrawIntLabel("seed", "Current Seed");
                
//                DrawProperty("wrapMode", "Wrap Axes Mode");

//                var obj = fieldInfo.GetValue(curPropBeingEdited.serializedObject.targetObject);
//                HeightMap heightMap = obj as HeightMap;
//                DrawPreviewTexture(heightMap.Texture);
//            }
//            --EditorGUI.indentLevel;

//            EditorGUI.EndProperty();
//        }

//        /// <summary>
//        /// Draws a label for an int property.
//        /// </summary>
//        /// <param name="propertyName"></param>
//        /// <param name="label"></param>
//        /// <param name="customHeight"></param>
//        void DrawIntLabel(string propertyName, string label, int customHeight = DEFAULT_HEIGHT)
//        {
//            Rect newRect = new Rect(drawerPosition);
//            newRect.y += (numOfItemsDrawn * customHeight);
//            newRect.height = customHeight;

//            SerializedProperty curProp = curPropBeingEdited.FindPropertyRelative(propertyName);
//            EditorGUI.SelectableLabel(newRect, string.Format("{0}\t{1}", label, curProp.intValue.ToString()));
//            ++numOfItemsDrawn;
//        }

//        /// <summary>
//        /// Draws unity default property.
//        /// </summary>
//        /// <param name="propertyName"></param>
//        /// <param name="label"></param>
//        /// <param name="customHeight"></param>
//        void DrawProperty(string propertyName, string label, int customHeight = DEFAULT_HEIGHT)
//        {
//            Rect newRect = new Rect(drawerPosition);
//            newRect.y += (numOfItemsDrawn * customHeight);
//            newRect.height = customHeight;

//            SerializedProperty curProp = curPropBeingEdited.FindPropertyRelative(propertyName);
//            EditorGUI.PropertyField(newRect, curProp, new GUIContent(label));
//            ++numOfItemsDrawn;            
//        }

//        /// <summary>
//        /// Draws a range for an int property.
//        /// </summary>
//        /// <param name="propertyName"></param>
//        /// <param name="label"></param>
//        /// <param name="min"></param>
//        /// <param name="max"></param>
//        /// <param name="customHeight"></param>
//        void DrawPropertyRange(string propertyName, string label, int min, int max, int customHeight = DEFAULT_HEIGHT)
//        {
//            Rect newRect = new Rect(drawerPosition);
//            newRect.y += (numOfItemsDrawn * customHeight);
//            newRect.height = customHeight;

//            SerializedProperty curProp = curPropBeingEdited.FindPropertyRelative(propertyName);
//            EditorGUI.IntSlider(newRect, curProp, min, max, new GUIContent(label));

//            ++numOfItemsDrawn;
//        }

//        /// <summary>
//        /// Draws a range for an float property.
//        /// </summary>
//        /// <param name="propertyName"></param>
//        /// <param name="label"></param>
//        /// <param name="min"></param>
//        /// <param name="max"></param>
//        /// <param name="customHeight"></param>
//        void DrawPropertyRange(string propertyName, string label, float min, float max, int customHeight = DEFAULT_HEIGHT)
//        {
//            Rect newRect = new Rect(drawerPosition);
//            newRect.y += (numOfItemsDrawn * customHeight);
//            newRect.height = customHeight;

//            SerializedProperty curProp = curPropBeingEdited.FindPropertyRelative(propertyName);
//            EditorGUI.Slider(newRect, curProp, min, max, new GUIContent(label));

//            ++numOfItemsDrawn;
//        }

//        /// <summary>
//        /// Draws a preview texture for the height map.
//        /// </summary>
//        /// <param name="textureToDraw"></param>
//        void DrawPreviewTexture(Texture2D textureToDraw)
//        {
//            if (textureToDraw == null)
//                return;
              
//            // calculate rect ~90% of inspector window width for drawing the texture
//            Rect newRect = new Rect(drawerPosition);
//            newRect.y += ((numOfItemsDrawn * DEFAULT_HEIGHT) + 10);
//            newRect.height = newRect.width - (newRect.width*.1f);
//            newRect.width -= (newRect.width * .1f);
//            texturePropertyRectHeight = newRect.height;
            
//            EditorGUI.DrawPreviewTexture(newRect, textureToDraw, null, ScaleMode.ScaleToFit, 0);
//        }

//        /// <summary>
//        /// Overrides Property height to ensure enough room for newly drawn components.
//        /// </summary>
//        /// <param name="property"></param>
//        /// <param name="label"></param>
//        /// <returns></returns>
//        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//        {
//            //Get the base height when not expanded
//            var height = base.GetPropertyHeight(property, label);
//            return height + (DEFAULT_HEIGHT * NUM_OF_PROPERTIES_IN_DRAWER) + texturePropertyRectHeight;
//        }

//    }

//}