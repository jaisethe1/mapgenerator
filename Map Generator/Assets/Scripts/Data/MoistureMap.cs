﻿
using AccidentalNoise;
using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    [Serializable]
    public class MoistureMap : BaseMap
    {
        [Header("Moisture Fractal")]
        /// <summary>
        /// Type of fractal noise algorithm to use to generate map.
        /// </summary>
        public FractalType fractalType = FractalType.MULTI;

        /// <summary>
        /// The basis type used in the noise algorithm.
        /// </summary>
        public BasisType basisType = BasisType.SIMPLEX;

        /// <summary>
        /// Type of interpolation used in the noise algorithm.
        /// </summary>
        public InterpolationType interpolationType = InterpolationType.QUINTIC;

        /// <summary>
        /// Number of octaves used to generate moisture data.
        /// </summary>
        [Range(1, 10)]
        public int moistureOctaves = 4;

        /// <summary>
        /// The frequency used when generating moisture data.
        /// </summary>
        //[Range(1, 10)]
        [ReadOnly]
        public float moistureFrequency = 3.0f;

        /// <summary>
        /// The fractal noise data to give moisture map some randomness.
        /// </summary>
        ImplicitFractal moistureFractal;

        [Header("Moisture Adjustment Based on Terrain")]
        public MoistureAdjustment moistureAdjustmentBasedOnTerrain = new MoistureAdjustment();

        /// <summary>
        /// Updates the moisture map with the given seed.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="seed"></param>
        public override void UpdateMap(int width, int height, int seed)
        {
            base.UpdateMap(width, height, seed);

            // initialize the Moisture map
            moistureFractal = new ImplicitFractal(fractalType,
                                                        basisType,
                                                        interpolationType,
                                                        moistureOctaves,
                                                        moistureFrequency,
                                                        seed);

            // extract the data with respect to the wrap mode filter.
            switch (wrapMode)
            {
                case WrapMode.NoWrap:
                    ExtractDataFromNoiseNoWrap(moistureFractal, ref mapData);
                    break;
                case WrapMode.WrapX:
                    ExtractDataFromNoiseWrapX(moistureFractal, ref mapData);
                    break;
                case WrapMode.WrapXY:
                    ExtractDataFromNoiseWrapXY(moistureFractal, ref mapData);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Creates a list of Tiles using the moisture map's fractal data
        /// </summary>
        /// <returns></returns>
        public Tile[,] CreateFractalTiles(bool forceMaskOff = false)
        {
            return CreateTiles(moistureFractal, forceMaskOff);
        }

        /// <summary>
        /// Creates a list of Tiles using the moisture map's combined data
        /// </summary>
        /// <returns></returns>
        /*public Tile[,] CreateCombinedTiles()
        {
            return CreateTiles(combinedMoistureNoiseData);
        }*/

        Tile[,] CreateTiles(ImplicitModuleBase data, bool forceMaskOff = false)
        {
            MapData tempMapData = new MapData(width, height);

            switch (wrapMode)
            {
                case WrapMode.NoWrap:
                    ExtractDataFromNoiseNoWrap(data, ref tempMapData, forceMaskOff);
                    break;
                case WrapMode.WrapX:
                    ExtractDataFromNoiseWrapX(data, ref tempMapData, forceMaskOff);
                    break;
                case WrapMode.WrapXY:
                    ExtractDataFromNoiseWrapXY(data, ref tempMapData, forceMaskOff);
                    break;
                default:
                    break;
            }

            Tile[,] tiles = new Tile[width, height];

            MoistureTypeConfiguration config = MoistureTypeConfiguration.DefaultConfig();
            if (MoistureTypeConfiguration.DefaultConfig() != null)
            {
                float moistureValue = 0;
                for (var x = 0; x < width; x++)
                {
                    for (var y = 0; y < height; y++)
                    {
                        Tile t = new Tile();
                        t.x = x;
                        t.y = y;

                        // find and set moisture value
                        moistureValue = tempMapData.data[x, y];
                        moistureValue = (moistureValue - tempMapData.min) / (tempMapData.max - tempMapData.min);
                        t.moistureValue = moistureValue;
                        
                        // set tile moisture type based on moisture config's threshold levels.
                        MoistureTypePair[] moistureTypes = config.moistureTypeOrdering;
                        for (int iCounter = 0; iCounter < moistureTypes.Length; ++iCounter)
                        {
                            if (moistureValue <= moistureTypes[iCounter].moistureValue)
                            {
                                t.moistureTypeData = moistureTypes[iCounter].moistureType;
                                t.moistureTypeEnum = (MoistureTypes)Enum.Parse(typeof(MoistureTypes), moistureTypes[iCounter].moistureType.name.ToString());
                                break;
                            }
                        }

                        tiles[x, y] = t;
                    }
                }
            }
            else
                Debug.LogError("No default Moisture Type Configuration available.");
            return tiles;
        }

        /// <summary>
        /// Updates a list of tiles' moisture values given a moisture map and the types of moisture
        /// </summary>
        /// <param name="mapTiles"></param>
        /// <param name="moistureMap"></param>
        /// <param name="moistureTypeConfig"></param>
        public static void UpdateTilesWithMoistureMap(ref Tile[,] mapTiles, MoistureMap moistureMap, MoistureTypeConfiguration moistureTypeConfig)
        {
            for (var x = 0; x < moistureMap.width; x++)
            {
                for (var y = 0; y < moistureMap.height; y++)
                {
                    UpdateTileWithMoisture(ref mapTiles[x, y], moistureMap, moistureTypeConfig);
                }
            }
        }

        /// <summary>
        /// Updates the amount of moisture and the type of moisture for a tiles.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="moistureMap"></param>
        /// <param name="moistureTypeConfig"></param>
        public static void UpdateTileWithMoisture(ref Tile t, MoistureMap moistureMap, MoistureTypeConfiguration moistureTypeConfig)
        {
            // adjust Moisture Map based on Height - Higher == colder
            MoistureAdjustmentPair adjustment;
            for (int iTerrainType = 0; iTerrainType < moistureMap.moistureAdjustmentBasedOnTerrain.terrainMoistureAdjustments.Length; ++iTerrainType)
            {
                adjustment = moistureMap.moistureAdjustmentBasedOnTerrain.terrainMoistureAdjustments[iTerrainType];
                if (t.terrainTypeEnum == adjustment.terrainType)
                {
                    moistureMap.mapData.data[t.x, t.y] += adjustment.moistureAdjustment * t.heightValue;
                    break;
                }
                if (iTerrainType == moistureMap.moistureAdjustmentBasedOnTerrain.terrainMoistureAdjustments.Length - 1)
                {
                    moistureMap.mapData.data[t.x, t.y] += moistureMap.moistureAdjustmentBasedOnTerrain.adjustmentForRemainderTerrainTypes * t.heightValue;
                }
            }

            // find and set moisture value
            float moistureValue = moistureMap.mapData.data[t.x, t.y];
            moistureValue = (moistureValue - moistureMap.mapData.min) / (moistureMap.mapData.max - moistureMap.mapData.min);
            moistureValue = Mathf.Clamp01(moistureValue);

            if (moistureTypeConfig != null && !float.IsNaN(moistureValue))
            {
                t.moistureValue = moistureValue;

                // set tile moisture type based on moisture config's threshold levels.
                MoistureTypePair[] moistureTypes = moistureTypeConfig.moistureTypeOrdering;
                for (int iCounter = 0; iCounter < moistureTypes.Length; ++iCounter)
                {
                    if (moistureValue <= moistureTypes[iCounter].moistureValue)
                    {
                        t.moistureTypeData = moistureTypes[iCounter].moistureType;
                        t.moistureTypeEnum = (MoistureTypes)Enum.Parse(typeof(MoistureTypes), moistureTypes[iCounter].moistureType.name.ToString());

                        break;
                    }
                }

                if (t.moistureTypeData == null)
                {
                    Debug.Log("A tile couldn't determine it's moisture type. Likely because MoistureType list doesn't contain a moisture type threshold with a value of 1.");
                }
            }
        }
    }

    /// <summary>
    /// Defines a moisture adjustment based on the terrain type of a tile.
    /// </summary>
    [Serializable]
    public class MoistureAdjustment
    {
        /// <summary>
        /// The terrains and how much adjustment they should receive.
        /// </summary>
        public MoistureAdjustmentPair[] terrainMoistureAdjustments;

        /// <summary>
        /// The amount of adjustment for all other terrain types not
        /// contained in the list. This helps even the values a bit.
        /// </summary>
        public float adjustmentForRemainderTerrainTypes = .01f;
    }

    /// <summary>
    /// Holds a terrain type and the amount of adjustment
    /// it should receive on a tile of the corresponding type.
    /// It then affects it's moisture index by the adjustment amount.
    /// </summary>
    [Serializable]
    public class MoistureAdjustmentPair
    {
        /// <summary>
        /// The type of terrain which should be adjusted.
        /// </summary>
        public TerrainTypes terrainType;

        /// <summary>
        /// The amount of moisture to adjust by.
        /// </summary>
        public float moistureAdjustment;
    }


}