﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Defines a base configuration which stores where the config's default config file
    /// can be found/stored. This is a design choice that was opted for.
    /// </summary>
    [Serializable]
    public class BaseTypeConfiguration : ScriptableObject
    {
        /// <summary>
        /// This flag is very important. Ideally, only one config of a type will be marked true.
        /// This is used when searching for the default config.
        /// More importantly, the flag is used to generate the Type enum associated with each config,
        /// meaning the default config should contain all possible types -- it's the master list.
        /// 
        /// This is false by default to prevent overwriting an already existing enum file.
        /// </summary>
        public bool isDefaultConfig = false;

        /// <summary>
        /// The type of enum to generate as values are add/removed.
        /// </summary>
        public string typeName = "BaseType";

        /// <summary>
        /// The path relative to Assets/ where the type enum will be created.
        /// </summary>
        public string typeEnumPath { get { return string.Format("/Data/{0} Data/{0}s.cs", typeName); } }
    }

}