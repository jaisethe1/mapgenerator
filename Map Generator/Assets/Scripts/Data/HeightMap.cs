﻿using AccidentalNoise;
using System;
using UnityEngine;

namespace Noodle.MapGenerator
{

    /// <summary>
    /// Defines the type of wrapping used when generating a map.
    /// </summary>
    public enum WrapMode
    {
        NoWrap,     // uses the noise data as-is
        WrapX,      // adjusts the noise data so the texture wraps in the x-axis
        WrapXY      // adjusts the noise data so the texture wraps in both x & y axes
    }

    /// <summary>
    /// Holds the data generated through noise that represents height.
    /// </summary>
    [Serializable]
    public class HeightMap : BaseMap
    {        
        [Header("Height Fractal")]
        /// <summary>
        /// Type of fractal noise algorithm to use to generate map.
        /// </summary>
        public FractalType fractalType = FractalType.HYBRIDMULTI;
        
        /// <summary>
        /// The basis type used in the noise algorithm.
        /// </summary>
        public BasisType basisType = BasisType.GRADIENT;
        
        /// <summary>
        /// Type of interpolation used in the noise algorithm.
        /// </summary>
        public InterpolationType interpolationType = InterpolationType.QUINTIC;

        /// <summary>
        /// Number of terrain octaves used to determine how vast the terrain is.
        /// Low number = smooth transitions
        /// High number = jagged/detailed transitions
        /// </summary>
        [Range(1, 10)]
        public int terrainOctaves = 6;

        /// <summary>
        /// How much noise is in the terrain v. how much it stays the same.
        /// Low number = smaller terrain size, less transitions
        /// High number = larger terrain size, more transitions
        /// </summary>
        //[Range(1, 10)]
        [ReadOnly]
        public double terrainFrequency = 1.25;

        /// <summary>
        /// The fractal noise data.
        /// </summary>
        public ImplicitFractal fractalNoiseData;

        /// <summary>
        /// Updates the height map in its entirety
        /// </summary>
        public override void UpdateMap(int width, int height, int seed)
        {
            base.UpdateMap(width, height, seed);

            fractalNoiseData = new ImplicitFractal(fractalType,
                                           basisType,
                                           interpolationType,
                                           terrainOctaves,
                                           terrainFrequency,
                                           seed);

            // extract the data with respect to the wrap mode filter.
            switch (wrapMode)
            {
                case WrapMode.NoWrap:
                    ExtractDataFromNoiseNoWrap(fractalNoiseData, ref mapData);
                    break;
                case WrapMode.WrapX:
                    ExtractDataFromNoiseWrapX(fractalNoiseData, ref mapData);
                    break;
                case WrapMode.WrapXY:
                    ExtractDataFromNoiseWrapXY(fractalNoiseData, ref mapData);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Iterates through all of the tiles and updates them with their corresponing heights based on the given
        /// height map and terrain types available.
        /// </summary>
        /// <param name="mapTiles">The list of terrain tiles to be updated.</param>
        /// <param name="heightMap">The heightmap object which defines the heights of the map.</param>
        /// <param name="terrainTypesConfig">The terrain types available which correlate to heights.</param>
        public static void UpdateTilesWithHeightMap(ref Tile[,] mapTiles, HeightMap heightMap, TerrainTypeConfiguration terrainTypesConfig)
        {
            for (var x = 0; x < heightMap.width; x++)
            {
                for (var y = 0; y < heightMap.height; y++)
                {
                    UpdateTileWithHeight(ref mapTiles[x, y], heightMap, terrainTypesConfig);
                }
            }
        }

        /// <summary>
        /// Sets the height value and terrain type based on terrain configuration for a given tile.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="heightMap"></param>
        /// <param name="terrainTypesConfig"></param>
        public static void UpdateTileWithHeight(ref Tile t, HeightMap heightMap, TerrainTypeConfiguration terrainTypesConfig)
        {
            if (heightMap != null)
            {
                // find and set height value
                float heightMapValue = heightMap.mapData.data[t.x, t.y];
                heightMapValue = (heightMapValue - heightMap.mapData.min) / (heightMap.mapData.max - heightMap.mapData.min);
                t.heightValue = heightMapValue;

                // set tile terrain type based on the terrain config's threshold levels.
                TerrainTypePair[] terrainTypes = terrainTypesConfig.terrainTypeOrdering;
                for (int iCounter = 0; iCounter < terrainTypes.Length; ++iCounter)
                {
                    if (heightMapValue <= terrainTypes[iCounter].terrainHeightThreshold && !float.IsNaN(heightMapValue) &&
                        terrainTypes[iCounter].terrainType != null)
                    {
                        t.terrainTypeEnum = (TerrainTypes)Enum.Parse(typeof(TerrainTypes), terrainTypes[iCounter].terrainType.name.ToString());
                        t.terrainTypeData = terrainTypes[iCounter].terrainType;
                        t.collidable = !terrainTypes[iCounter].terrainType.isWater;
                        break;
                    }
                }

                // handling a bug/issue I encountered prior when last item in the 
                // TerrainType list was .9 instead of 1 and caused some undesired results.
                if (t.terrainTypeData == null)
                {
                    Debug.Log("A tile couldn't determine it's terrain type. Likely because TerrainType list doesn't contain a terrain type threshold with a value of 1");
                }
            }
        }
    }

}