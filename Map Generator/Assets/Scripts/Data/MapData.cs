﻿
namespace Noodle.MapGenerator
{
    /// <summary>
    /// Holds value data for a given map (IE height).
    /// </summary>
    public class MapData
    {
        /// <summary>
        /// The data mapping.
        /// </summary>
        public float[,] data;

        /// <summary>
        /// The min value in the map.
        /// </summary>
        public float min;

        /// <summary>
        /// The max value in the map.
        /// </summary>
        public float max;

        /// <summary>
        /// Inits a default map with given width and height.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public MapData(int width, int height)
        {
            data = new float[width, height];
            min = float.MaxValue;
            max = float.MinValue;
        }
    }
}