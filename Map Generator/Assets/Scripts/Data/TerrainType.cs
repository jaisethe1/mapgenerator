﻿using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// An asset that defines a terrain type.
    /// </summary>
    [Serializable]
    public class TerrainType : ScriptableObject
    {
        /// <summary>
        /// The color corresponding to the terrain type.
        /// </summary>
        public Color color;

        /// <summary>
        /// True if the terrain type is water-based.
        /// False otherwise.
        /// </summary>
        public bool isWater = false;
    }

}