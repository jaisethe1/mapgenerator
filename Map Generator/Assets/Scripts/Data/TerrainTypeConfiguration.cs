﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Defines a configuration of terrain types which determine ordering.
    /// </summary>
    [Serializable]
    public class TerrainTypeConfiguration : BaseTypeConfiguration
    {
        /// <summary>
        /// The ordering of terrain types.
        /// </summary>
        public TerrainTypePair[] terrainTypeOrdering;  
        
        /// <summary>
        /// Gets an array of colors of the terrain from lowest to highest heights.
        /// </summary>
        public Color[] TerrainColors
        {
            get
            {
                Color[] colors = new Color[terrainTypeOrdering.Length];
                for(int iColor = 0; iColor < colors.Length; ++iColor)
                {
                    colors[iColor] = terrainTypeOrdering[iColor].terrainType.color;
                }
                return colors;
            }
        }

        /// <summary>
        /// Gets an array of colors of the terrain from lowest to highest heights.
        /// </summary>
        public float[] TerrainHeightThresholds
        {
            get
            {
                float[] heights = new float[terrainTypeOrdering.Length];
                for (int iHeight = 0; iHeight < heights.Length; ++iHeight)
                {
                    heights[iHeight] = terrainTypeOrdering[iHeight].terrainHeightThreshold;
                }
                return heights;
            }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Finds and loads the first default config file we can find.
        /// </summary>
        /// <returns></returns>
        public static TerrainTypeConfiguration DefaultConfig()
        {
            string[] terrainConfigs = AssetDatabase.FindAssets("t:TerrainTypeConfiguration");
            if (terrainConfigs.Length > 0)
            {
                string filePath;
                TerrainTypeConfiguration config;
                for (int iPath = 0; iPath < terrainConfigs.Length; ++iPath)
                {
                    filePath = AssetDatabase.GUIDToAssetPath(terrainConfigs[iPath]);
                    config = AssetDatabase.LoadAssetAtPath<TerrainTypeConfiguration>(filePath);
                    if (config != null && config.isDefaultConfig)
                        return config;
                }
            }
            Debug.LogWarning("No default TerrainTypeConfiguration to load. You should create one.");
            return null;
        }

#endif

    }

    /// <summary>
    /// Pairs a TerrainType asset with a height threshold value.
    /// This allows for multiple configurations of terrains.
    /// </summary>
    [Serializable]     public class TerrainTypePair
    {
        /// <summary>
        /// The terrain type associated.
        /// </summary>
        public TerrainType terrainType;

        /// <summary>
        /// The height threshold for this terrain.
        /// </summary>
        [Range(0, 1)]
        public float terrainHeightThreshold;
    }

}