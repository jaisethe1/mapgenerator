﻿using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// An asset that defines a moisture type.
    /// </summary>
    [Serializable]
    public class MoistureType : ScriptableObject
    {
        /// <summary>
        /// The color indicating the moisture level (dry v wet?)
        /// </summary>
        public Color color;
    }

    /// <summary>
    /// Pairs a MoistureType asset with a moisture value.
    /// This allows for multiple configurations of moisture.
    /// </summary>
    [Serializable]     public class MoistureTypePair
    {
        /// <summary>
        /// The moisture type associated.
        /// </summary>
        public MoistureType moistureType;

        /// <summary>
        /// The value associated with this moisture.
        /// </summary>
        [Range(0, 1)]
        public float moistureValue;
    }

}