﻿using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// An asset that defines a heat type.
    /// </summary>
    [Serializable]
    public class HeatType : ScriptableObject
    {
        /// <summary>
        /// The color indicating the heat (warm or cool?)
        /// </summary>
        public Color color;
    }

    /// <summary>
    /// Pairs a HeatType asset with a heat value.
    /// This allows for multiple configurations of heat.
    /// </summary>
    [Serializable]     public class HeatTypePair
    {
        /// <summary>
        /// The heat type associated.
        /// </summary>
        public HeatType heatType;

        /// <summary>
        /// The value associated with this heat.
        /// </summary>
        [Range(0, 1)]
        public float heatIndex;
    }

}