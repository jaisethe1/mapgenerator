﻿
using AccidentalNoise;
using System;
using UnityEngine;

namespace Noodle.MapGenerator
{
    [Serializable]
    public class HeatMap : BaseMap
    {
        [Header("Heat Gradient")]
        /// <summary>
        /// The heat gradient.
        /// </summary>
        public ImplicitGradientPair heatGradient = new ImplicitGradientPair();

        [Header("Heat Fractal")]
        /// <summary>
        /// Type of fractal noise algorithm to use to generate map.
        /// </summary>
        public FractalType fractalType = FractalType.MULTI;

        /// <summary>
        /// The basis type used in the noise algorithm.
        /// </summary>
        public BasisType basisType = BasisType.SIMPLEX;

        /// <summary>
        /// Type of interpolation used in the noise algorithm.
        /// </summary>
        public InterpolationType interpolationType = InterpolationType.QUINTIC;

        /// <summary>
        /// Number of octaves used to generate heat data.
        /// </summary>
        [Range(1, 10)]
        public int heatOctaves = 4;

        /// <summary>
        /// The frequency used when generating heat data.
        /// </summary>
        //[Range(1, 10)]
        [ReadOnly]
        public float heatFrequency = 3.0f;

        /// <summary>
        /// The fractal noise data to give heat map some randomness.
        /// </summary>
        ImplicitFractal heatFractal;

        [Header("Combined Heat Data")]
        /// <summary>
        /// The method of combining the heat maps together.
        /// </summary>
        public CombinerType combinerType = CombinerType.AVERAGE;

        /// <summary>
        /// The combined data of the heat gradient and heat fractal noise datas.
        /// </summary>
        public ImplicitCombiner combinedHeatNoiseData;        

        [Header("Heat Adjustment Based on Terrain")]
        public HeatAdjustment heatAdjustmentBasedOnTerrain = new HeatAdjustment();

        /// <summary>
        /// Updates the heat map with the given seed.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="seed"></param>
        public override void UpdateMap(int width, int height, int seed)
        {
            base.UpdateMap(width, height, seed);

            // initialize the Heat map
            // this creates horizontal gradients from 'hot' to 'cold' ~(1 to 0) 
            // EX:
            //      1.0 1.0 1.0 1.0 1.0
            //      0.8 0.8 0.8 0.8 0.8
            //      0.4 0.4 0.4 0.4 0.4
            //      0.0 0.0 0.0 0.0 0.0
            ImplicitGradientParams p = heatGradient.gradientParams;
            heatGradient.gradient.SetGradient(p.X0, p.X1, p.Y0, p.Y1, p.Z0, p.Z1, p.W0, p.W1, p.U0, p.U1, p.V0, p.V1);

            heatFractal = new ImplicitFractal(fractalType,
                                                basisType,
                                                interpolationType,
                                                heatOctaves,
                                                heatFrequency,
                                                seed);

            // combine the gradient with our heat fractal
            // this provides some randomness to the heat gradient.
            combinedHeatNoiseData = new ImplicitCombiner(combinerType);
            combinedHeatNoiseData.AddSource(heatGradient.gradient);
            combinedHeatNoiseData.AddSource(heatFractal);

            // extract the data with respect to the wrap mode filter.
            switch (wrapMode)
            {
                case WrapMode.NoWrap:
                    ExtractDataFromNoiseNoWrap(combinedHeatNoiseData, ref mapData);
                    break;
                case WrapMode.WrapX:
                    ExtractDataFromNoiseWrapX(combinedHeatNoiseData, ref mapData);
                    break;
                case WrapMode.WrapXY:
                    ExtractDataFromNoiseWrapXY(combinedHeatNoiseData, ref mapData);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Creates a list of Tiles using the heat map's Gradient data
        /// </summary>
        /// <returns></returns>
        public Tile[,] CreateGradientTiles(bool forceMaskOff = false)
        {
            return CreateTiles(heatGradient.gradient, forceMaskOff);
        }

        /// <summary>
        /// Creates a list of Tiles using the heat map's fractal data
        /// </summary>
        /// <returns></returns>
        public Tile[,] CreateFractalTiles(bool forceMaskOff = false)
        {
            return CreateTiles(heatFractal, forceMaskOff);
        }

        /// <summary>
        /// Creates a list of Tiles using the heat map's combined data
        /// </summary>
        /// <returns></returns>
        public Tile[,] CreateCombinedTiles(bool forceMaskOff = false)
        {
            return CreateTiles(combinedHeatNoiseData, forceMaskOff);
        }

        Tile[,] CreateTiles(ImplicitModuleBase data, bool forceMaskOff = false)
        {
            MapData tempMapData = new MapData(width, height);

            switch (wrapMode)
            {
                case WrapMode.NoWrap:
                    ExtractDataFromNoiseNoWrap(data, ref tempMapData, forceMaskOff);
                    break;
                case WrapMode.WrapX:
                    ExtractDataFromNoiseWrapX(data, ref tempMapData, forceMaskOff);
                    break;
                case WrapMode.WrapXY:
                    ExtractDataFromNoiseWrapXY(data, ref tempMapData, forceMaskOff);
                    break;
                default:
                    break;
            }

            Tile[,] tiles = new Tile[width, height];

            HeatTypeConfiguration config = HeatTypeConfiguration.DefaultConfig();
            if (HeatTypeConfiguration.DefaultConfig() != null)
            {
                float heatValue = 0;
                for (var x = 0; x < width; x++)
                {
                    for (var y = 0; y < height; y++)
                    {
                        Tile t = new Tile();
                        t.x = x;
                        t.y = y;

                        // find and set heat value
                        heatValue = tempMapData.data[x, y];
                        heatValue = (heatValue - tempMapData.min) / (tempMapData.max - tempMapData.min);
                        t.heatValue = heatValue;

                        // set tile heat type based on heat config's threshold levels.
                        HeatTypePair[] heatTypes = config.heatTypeOrdering;
                        for (int iCounter = 0; iCounter < heatTypes.Length; ++iCounter)
                        {
                            if (heatValue <= heatTypes[iCounter].heatIndex)
                            {
                                t.heatTypeData = heatTypes[iCounter].heatType;
                                t.heatTypeEnum = (HeatTypes)Enum.Parse(typeof(HeatTypes), heatTypes[iCounter].heatType.name.ToString());
                                break;
                            }
                        }

                        tiles[x, y] = t;
                    }
                }
            }
            else
                Debug.LogError("No default Heat Type Configuration available.");
            return tiles;
        }

        /// <summary>
        /// Updates a list of tiles with a given heat map and heat types.
        /// </summary>
        /// <param name="mapTiles"></param>
        /// <param name="heatMap"></param>
        /// <param name="heatTypeConfig"></param>
        public static void UpdateTilesWithHeatMap(ref Tile[,] mapTiles, HeatMap heatMap, HeatTypeConfiguration heatTypeConfig)
        {
            for (var x = 0; x < heatMap.width; x++)
            {
                for (var y = 0; y < heatMap.height; y++)
                {
                    UpdateTileWithHeat(ref mapTiles[x, y], heatMap, heatTypeConfig);
                }
            }
        }

        /// <summary>
        /// Updates the heat value and heat type based on the heat type configuration for a tile.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="heatMap"></param>
        /// <param name="heatTypeConfig"></param>
        public static void UpdateTileWithHeat(ref Tile t, HeatMap heatMap, HeatTypeConfiguration heatTypeConfig)
        {
            // Adjust Heat Map based on Height - Higher == colder
            HeatAdjustmentPair adjustment;
            for (int iTerrainType = 0; iTerrainType < heatMap.heatAdjustmentBasedOnTerrain.terrainHeatAdjustments.Length; ++iTerrainType)
            {
                adjustment = heatMap.heatAdjustmentBasedOnTerrain.terrainHeatAdjustments[iTerrainType];
                if (t.terrainTypeEnum == adjustment.terrainType)
                {
                    heatMap.mapData.data[t.x, t.y] += adjustment.heatAdjustment * t.heightValue;
                    break;
                }
                if (iTerrainType == heatMap.heatAdjustmentBasedOnTerrain.terrainHeatAdjustments.Length - 1)
                {
                    heatMap.mapData.data[t.x, t.y] += heatMap.heatAdjustmentBasedOnTerrain.adjustmentForRemainderTerrainTypes * t.heightValue;
                }
            }

            // find and set heat value
            float heatValue = heatMap.mapData.data[t.x, t.y];
            heatValue = (heatValue - heatMap.mapData.min) / (heatMap.mapData.max - heatMap.mapData.min);
            heatValue = Mathf.Clamp01(heatValue);

            if (heatTypeConfig != null && !float.IsNaN(heatValue))
            {
                t.heatValue = heatValue;

                // set tile heat type based on heat config's threshold levels.
                HeatTypePair[] heatTypes = heatTypeConfig.heatTypeOrdering;
                for (int iCounter = 0; iCounter < heatTypes.Length; ++iCounter)
                {
                    if (heatValue <= heatTypes[iCounter].heatIndex)
                    {
                        t.heatTypeData = heatTypes[iCounter].heatType;
                        t.heatTypeEnum = (HeatTypes)Enum.Parse(typeof(HeatTypes), heatTypes[iCounter].heatType.name.ToString());

                        break;
                    }
                }

                if (t.heatTypeData == null)
                {
                    Debug.Log("A tile couldn't determine it's heat type. Likely because HeatType list doesn't contain a heat type threshold with a value of 1.");
                }
            }
        }
    }

    /// <summary>
    /// Defines a heat adjustment based on the terrain type of a tile.
    /// </summary>
    [Serializable]
    public class HeatAdjustment
    {
        /// <summary>
        /// The terrains and how much adjustment they should receive.
        /// </summary>
        public HeatAdjustmentPair[] terrainHeatAdjustments;

        /// <summary>
        /// The amount of adjustment for all other terrain types not
        /// contained in the list. This helps even the values a bit.
        /// </summary>
        public float adjustmentForRemainderTerrainTypes = .01f;
    }

    /// <summary>
    /// Holds a terrain type and the amount of adjustment
    /// it should receive on a tile of the corresponding type.
    /// It then affects it's heat index by the adjustment amount.
    /// </summary>
    [Serializable]
    public class HeatAdjustmentPair
    {
        /// <summary>
        /// The type of terrain which should recieve a heat adjustment.
        /// </summary>
        public TerrainTypes terrainType;

        /// <summary>
        /// The amount of heat adjustment that should be given.
        /// </summary>
        public float heatAdjustment;
    }


}