﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Defines a configuration for heat map thresholds.
    /// </summary>
    [Serializable]
    public class HeatTypeConfiguration : BaseTypeConfiguration
    {
        /// <summary>
        /// Ordering of heat types.
        /// </summary>
        public HeatTypePair[] heatTypeOrdering;

#if UNITY_EDITOR

        /// <summary>
        /// Finds and loads the first config file we can find.
        /// </summary>
        /// <returns></returns>
        public static HeatTypeConfiguration DefaultConfig()
        {
            string[] terrainConfigs = AssetDatabase.FindAssets("t:HeatTypeConfiguration");
            if (terrainConfigs.Length > 0)
            {
                string filePath;
                HeatTypeConfiguration config;
                for (int iPath = 0; iPath < terrainConfigs.Length; ++iPath)
                {
                    filePath = AssetDatabase.GUIDToAssetPath(terrainConfigs[iPath]);
                    config = AssetDatabase.LoadAssetAtPath<HeatTypeConfiguration>(filePath);
                    if (config != null && config.isDefaultConfig)
                        return config;
                }
            }
            Debug.LogWarning("No default HeatTypeConfiguration to load. You should create one.");
            return null;
        }

#endif

    }

}