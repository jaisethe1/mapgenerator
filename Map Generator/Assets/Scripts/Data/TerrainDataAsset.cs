﻿using System;
using UnityEngine;
using Noodle.MapGenerator;
using TerrainData = Noodle.MapGenerator.TerrainData;

/// <summary>
/// Wraps a terrain data object into an asset so it can be saved.
/// </summary>
[Serializable]
public class TerrainDataAsset : ScriptableObject
{
    public TerrainData terrainData;

    private void OnEnable()
    {
        if (terrainData == null)
            return;

        terrainData.UpdateMaps(true);
    }
}
