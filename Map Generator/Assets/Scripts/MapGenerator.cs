﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Collections;

#if UNITY_EDITOR
using System.Diagnostics;
using Debug = UnityEngine.Debug;
#endif

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Creates a tile map for terrain data based on a variety of
    /// parameters. It can use height, moisture, heat, etc to
    /// generate realistic terrains.
    /// </summary>
    [ExecuteInEditMode]
    public class MapGenerator : MonoBehaviour
    {
        /// <summary>
        /// The width/height of a single map.
        /// 
        /// The max vertices of a mesh in Unity is 65025 or 255^2.
        /// By setting the map chunk size to 241, it allows the ability to create a mesh with
        /// 240 vertices individually, or to skip every 2, 4, 6, 8, 10, 12 for LOD and faster generation.
        /// </summary>
        public const int MAP_CHUNK_SIZE = 241;

        [Header("Map Data")]

        [SerializeField]
        public TerrainDataAsset terrainDataAsset;

        /// <summary>
        /// All of the data associated with a piece of terrain.
        /// Data regarding height, heat, moisture, etc.
        /// </summary>
        [SerializeField]
        public TerrainData terrainDataModel = new TerrainData(MAP_CHUNK_SIZE);
        

        [Header("Map Texture Renderers")]
        /// <summary>
        /// The renderer used to draw the height map in black and white.
        /// </summary>
        [SerializeField]
        private MeshRenderer heightMapBWRenderer = null;

        /// <summary>
        /// The renderer used to draw the height map.
        /// </summary>
        [SerializeField]
        private MeshRenderer heightMapRenderer = null;

        /// <summary>
        /// The renderer used to draw the tile group map.
        /// </summary>
        [SerializeField]
        private MeshRenderer tileGroupMapRenderer = null;

        /// <summary>
        /// The renderer used to draw the heat gradient.
        /// </summary>
        [SerializeField]
        private MeshRenderer heatGradientRenderer = null;

        /// <summary>
        /// The renderer used to draw the heat map bands.
        /// </summary>
        [SerializeField]
        private MeshRenderer heatMapBandsRenderer = null;

        /// <summary>
        /// The renderer used to draw the heat map fratal data.
        /// </summary>
        [SerializeField]
        private MeshRenderer heatMapFractalRenderer = null;

        /// <summary>
        /// The renderer used to draw the heat map fractal and gradient data.
        /// </summary>
        [SerializeField]
        private MeshRenderer heatMapCombinedRenderer = null;

        /// <summary>
        /// The renderer used to draw the heat map combined on top of height map.
        /// </summary>
        [SerializeField]
        private MeshRenderer heatMapAdjustedForHeightRenderer = null;

        /// <summary>
        /// The renderer used to draw the moisture basic map.
        /// </summary>
        [SerializeField]
        private MeshRenderer moistureMapBasicRenderer = null;

        /// <summary>
        /// The renderer used to draw the moisture fractal map.
        /// </summary>
        [SerializeField]
        private MeshRenderer moistureMapFractalRenderer = null;

        /// <summary>
        /// The renderer used to draw the moisture adjusted map.
        /// </summary>
        [SerializeField]
        private MeshRenderer moistureMapAdjustedRenderer = null;      

        [Header("Map Update Flags")]

        /// <summary>
        /// The flag indicating if the data/mesh should be updated automatically when there is a change.
        /// </summary>
        public bool autoUpdate = true;

        /// <summary>
        /// A queue consisting of 'onFinished' handlers and their parameters associated to 
        /// completion of terrain data creation threads.
        /// </summary>
        private Queue<MapThreadInfo<TerrainData>> terrainDataThreadInfoQueue = new Queue<MapThreadInfo<TerrainData>>();

        /// <summary>
        /// A queue consisting of 'onFinished' handlers and their parameters associated to 
        /// completion of mesh data creation threads.
        /// </summary>
        private Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQueue = new Queue<MapThreadInfo<MeshData>>();

        /// <summary>
        /// Finds the required components needed to generate a map mesh.
        /// </summary>
        private void OnEnable()
        {
            if (terrainDataAsset != null)
            {
                terrainDataAsset.terrainData.mapSize = MAP_CHUNK_SIZE;
                terrainDataModel = terrainDataAsset.terrainData;

                // when Unity reloads (in editor) or when game first starts, we want to update
                // the terrain data so the maps and tiles are intialized and such to prevent
                // any null-ref-ex that could occur.
                terrainDataModel.UpdateMaps(true);
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.update += NotifyOfUpdatedValues;
#endif
        }

        public void NotifyOfUpdatedValues()
        {
            UnityEditor.EditorApplication.update -= NotifyOfUpdatedValues;

            MapTerrain[] terrains = GameObject.FindObjectsOfType<MapTerrain>();
            foreach (MapTerrain terrain in terrains)
            {
                terrain.GetTerrainData(true);
            }
        }

        /// <summary>
        /// Check to see if any threads have completed and their handlers need to be called.
        /// </summary>
        private void Update()
        {
            CheckThreadQueue();
        }

        #region Check Thread Completion and Notify

        /// <summary>
        /// Iterates through the queue of completed thread handlers and call those handlers with
        /// any associated data required for those handlers to execute.
        /// </summary>
        public void CheckThreadQueue()
        {
            // process any completed terrain data threads
            if (terrainDataThreadInfoQueue.Count > 0)
            {
                for (int i = 0; i < terrainDataThreadInfoQueue.Count; ++i)
                {
                    MapThreadInfo<TerrainData> threadInfo = terrainDataThreadInfoQueue.Dequeue();
                    if(!Application.isPlaying)
                    {
                        NotifyTerrainDataThreadComplete(threadInfo);
                    }
                    else
                    {
                        StartCoroutine(NotifyTerrainDataThreadCompleteAsCoroutine(threadInfo));
                    }
                }
            }

            // process any completed mesh data threads
            if (meshDataThreadInfoQueue.Count > 0)
            {
                for (int i = 0; i < meshDataThreadInfoQueue.Count; ++i)
                {
                    MapThreadInfo<MeshData> threadInfo = meshDataThreadInfoQueue.Dequeue();
                    threadInfo.callback(threadInfo.parameter);
                }
            }
        }

        /// <summary>
        /// Notifies the 'thread complete' handler that the terrain data thread has completed
        /// and gives it the newly created terrain data.
        /// </summary>
        /// <param name="threadInfo">The info from the completed thread containing results of operation and who to
        /// notify of them.</param>
        private void NotifyTerrainDataThreadComplete(MapThreadInfo<TerrainData> threadInfo)
        {
            // update the terrain tiles
            threadInfo.parameter.UpdateMaps(true);
            threadInfo.parameter.UpdateTileData();

            // create any associated map textures if conditions are right. This step may not be needed and will thus, simply return.
            CreateMapTextures(threadInfo.parameter);

            threadInfo.callback(threadInfo.parameter);
        }

        /// <summary>
        /// Notifies the 'thread complete' handler that the terrain data thread has completed
        /// and gives it the newly created terrain data as a coroutine to prevent Main Thread hiccups.
        /// </summary>
        /// <param name="threadInfo">The info from the completed thread containing results of operation and who to
        /// notify of them.</param>
        private IEnumerator NotifyTerrainDataThreadCompleteAsCoroutine(MapThreadInfo<TerrainData> threadInfo)
        {
            NotifyTerrainDataThreadComplete(threadInfo);

            yield return null;
        }

        #endregion

        #region Terrain Data Generator Thread

        /// <summary>
        /// Step 1:
        ///     The starting function to create a terrain data given.
        /// </summary>
        /// <param name="numTerrainChunks">The number of terrain chunks in one direction. IE. 3 = 3x3 = 9 total chunks.</param>
        /// <param name="onFinishedHandler">The handler to notify when thread has completed.</param>
        public void StartTerrainDataGeneratorThread(int numTerrainChunks, Action<TerrainData> onFinishedHandler)
        {
            GenerateTerrainDataInThread(numTerrainChunks, onFinishedHandler);
        }

        /// <summary>
        /// Starts a thread which creates and updates a terrain data based
        /// on the generator's model data.
        /// </summary>
        /// <param name="numTerrainChunks">The number of terrain chunks in one direction. IE. 3 = 3x3 = 9 total chunks.</param>
        /// <param name="onFinishedHandler">The handler to notify once the thread has completed.</param>
        private void GenerateTerrainDataInThread(int numTerrainChunks, Action<TerrainData> onFinishedHandler)
        {
            //TODO: Not sure if this is really needed now. Modifying heights not really way to deal with changing map size I don't think.            
                TerrainTypeConfiguration terrainTypesConfig = ScriptableObject.CreateInstance<TerrainTypeConfiguration>();
                terrainTypesConfig.terrainTypeOrdering = new TerrainTypePair[terrainDataAsset.terrainData.terrainTypesConfig.terrainTypeOrdering.Length];
                for (int i = 0; i < terrainDataAsset.terrainData.terrainTypesConfig.terrainTypeOrdering.Length; ++i)
                {
                    TerrainTypePair sourcePair = terrainDataAsset.terrainData.terrainTypesConfig.terrainTypeOrdering[i];
                    terrainTypesConfig.terrainTypeOrdering[i] = new TerrainTypePair
                    {
                        terrainType = sourcePair.terrainType,
                        terrainHeightThreshold = sourcePair.terrainHeightThreshold
                    };
                }
            

            // starts a thread to create the terrain data
            ThreadStart threadStart = delegate
            {
                CreateTerrainData(terrainDataModel, numTerrainChunks, onFinishedHandler, terrainTypesConfig);
            };

            new Thread(threadStart).Start();
        }

        /// <summary>
        /// Creates the terrain data and updates its map. Then adds itself to 'finished thread'
        /// queue so it can notify its handler.
        /// </summary>
        /// <param name="dataModel">The terrain data model that will be referenced as a starting point model for a new data.</param>
        /// <param name="numTerrainChunks">The number of terrain chunks in one direction. IE. 3 = 3x3 = 9 total chunks.</param>
        /// <param name="onFinished">The handler to notify once the thread has completed.</param>
        /// <param name="terrainTypesConfig">The terrain types config.</param>
        private void CreateTerrainData(TerrainData dataModel, int numTerrainChunks, Action<TerrainData> onFinishedHandler, TerrainTypeConfiguration terrainTypesConfig)
        {
            // create and update the terrain maps
            TerrainData terrainData = new TerrainData(dataModel, true, terrainTypesConfig, numTerrainChunks);

            // WARNING: Can't figure out why calling UpdateMaps in thread fails to yeild results
            // when calling on generation, all tiles are the same even though they have different threads.
            // Only when calling UpdateMaps outside of thread does it work. This is sad b/c UpdateMaps
            // is one of the more expensive functions which will slow down main thread.
            //terrainData.UpdateMaps();

            // add the completed data and the handler to notify of this new data to the thread-queue which will be handled
            // by the main thread I.E. unity thread.
            lock (terrainDataThreadInfoQueue)
            {
                terrainDataThreadInfoQueue.Enqueue(new MapThreadInfo<TerrainData>(terrainData, onFinishedHandler));
            }
        }

        #endregion

        #region Mesh Generator Thread

        /// <summary>
        /// Step 2:
        ///     The starting function to create a terrain mesh via threading given the terrain data.
        /// </summary>
        /// <param name="terrainData">The terrain data to reference for the creation of the mesh chunk.</param>
        /// <param name="terrainChunkIndex">The 0-index chunk index.</param>
        /// <param name="terrainChunkSize">The size of the terrain chunk.</param>
        /// <param name="onFinishedHandler">The handler to notify once the thread has completed.</param>
        public void StartMeshDataGeneratorThread(TerrainData terrainData, Vector2 terrainChunkIndex, int terrainChunkSize, Action<MeshData> onFinishedHandler)
        {
            GenerateTerrainMeshInThread(terrainData, terrainChunkIndex, terrainChunkSize, onFinishedHandler);
        }

        /// <summary>
        /// Generates the terrain in a thread
        /// </summary>
        /// <param name="terrainData">The terrain data to reference for the creation of the mesh chunk.</param>
        /// <param name="terrainChunkIndex">The 0-index chunk index.</param>
        /// <param name="terrainChunkSize">The size of the terrain chunk.</param>
        /// <param name="onFinishedHandler">The handler to notify once the thread has completed.</param>
        private void GenerateTerrainMeshInThread(TerrainData terrainData, Vector2 terrainChunkIndex, int terrainChunkSize, Action<MeshData> onFinishedHandler)
        {
            // starts a thread to create the mesh data
            // and subsequently create the mesh upon thread completion.
            ThreadStart threadStart = delegate
            {
                CreateMapMeshData(terrainData, terrainChunkIndex, terrainChunkSize, onFinishedHandler);
            };

            new Thread(threadStart).Start();
        }

        /// <summary>
        /// Creates a map mesh based on the map data.
        /// </summary>
        /// <param name="terrainData">The terrain data to reference for the creation of the mesh chunk.</param>
        /// <param name="terrainChunkIndex">The 0-index chunk index.</param>
        /// <param name="terrainChunkSize">The size of the terrain chunk.</param>
        /// <param name="onFinishedHandler">The handler to notify once the thread has completed.</param>
        private void CreateMapMeshData(TerrainData terrainData, Vector2 terrainChunkIndex, int terrainChunkSize, Action<MeshData> onFinishedHandler)
        {
            MeshData meshData = MeshGenerator.GenerateTerrainMesh(ref terrainData.mapTiles, terrainChunkIndex, terrainChunkSize, 
                terrainData.heightMultiplier, terrainData.levelOfDetail, terrainData.heightCurve);

            // add the completed data and the handler to notify of this new data to the thread-queue which will be handled
            // by the main thread I.E. unity thread.
            lock (meshDataThreadInfoQueue)
            {
                meshDataThreadInfoQueue.Enqueue(new MapThreadInfo<MeshData>(meshData, onFinishedHandler));
            }
        }

        #endregion

        #region Draw Map Textures To Renderers

        /// <summary>
        /// Create the map textures based on current data.
        /// </summary>
        /// <param name="terrainData">The terrain data to reference for the creation of the map textures.</param>
        public void CreateMapTextures(TerrainData terrainData)
        {
            if(terrainData.mapTiles == null || terrainData.mapTiles.Length == 0)
            {
                terrainData.UpdateMaps(true);
                terrainData.UpdateTileData();
            }
            DrawMapTypes(terrainData, terrainData.saveLoadTextures, terrainData.createAllTextureForms);
        }

        /// <summary>
        /// Draws all of the various map types to their respective renderers.
        /// </summary>
        /// <param name="terrainData">The data to use for generating terrain textures.</param>
        /// <param name="saveLoadTextures">The flag specifying to save out textures to resources dir.</param>
        /// <param name="createAllTextureForms">The flag indicating to render all textures to their respective renderers.</param>
        private void DrawMapTypes(TerrainData terrainData, bool saveLoadTextures = false, bool createAllTextureForms = false)
        {
            if (heightMapRenderer != null)
            {
                Texture2D tex = TextureGenerator.CreateTerrainTypeTexture(terrainData.mapSize, terrainData.mapSize, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heightMapRenderer.png");
                heightMapRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (heightMapBWRenderer != null)
            {
                Texture2D tex = TextureGenerator.CreateHeightBlackWhiteTexture(terrainData.mapSize, terrainData.mapSize, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heightMapBWRenderer.png");
                heightMapBWRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            // don't waste time/resources creating textures of various forms (moisture, heat) and their permuations
            // unless it is desired.
            if (!createAllTextureForms)
                return;

            if (tileGroupMapRenderer != null)
            {
                Texture2D tex = TextureGenerator.CreateLandMassTexture(terrainData.mapSize, terrainData.mapSize, ref terrainData.waterTileGroups, ref terrainData.landTileGroups, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/tileGroupMapRenderer.png");
                tileGroupMapRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (heatGradientRenderer != null)
            {
                Tile[,] heatMapGradientTiles = terrainData.heatMap.CreateGradientTiles();
                Texture2D tex = TextureGenerator.CreateHeatMapTexture(terrainData.mapSize, terrainData.mapSize, ref heatMapGradientTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heatGradientRenderer.png");
                heatGradientRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (heatMapBandsRenderer != null)
            {
                Tile[,] heatMapTiles = terrainData.heatMap.CreateGradientTiles();
                Texture2D tex = TextureGenerator.CreateHeatTypeMapTexture(terrainData.mapSize, terrainData.mapSize, ref heatMapTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heatMapBandsRenderer.png");
                heatMapBandsRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (heatMapFractalRenderer != null)
            {
                Tile[,] heatMapTiles = terrainData.heatMap.CreateFractalTiles();
                Texture2D tex = TextureGenerator.CreateHeatTypeMapTexture(terrainData.mapSize, terrainData.mapSize, ref heatMapTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heatMapFractalRenderer.png");
                heatMapFractalRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (heatMapCombinedRenderer != null)
            {
                Tile[,] heatMapTiles = terrainData.heatMap.CreateCombinedTiles();
                Texture2D tex = TextureGenerator.CreateHeatTypeMapTexture(terrainData.mapSize, terrainData.mapSize, ref heatMapTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heatMapCombinedRenderer.png");
                heatMapCombinedRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (heatMapAdjustedForHeightRenderer != null)
            {
                Texture2D tex = TextureGenerator.CreateHeatTypeMapTexture(terrainData.mapSize, terrainData.mapSize, ref terrainData.mapTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/heatMapAdjustedForHeightRenderer.png");
                heatMapAdjustedForHeightRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (moistureMapBasicRenderer != null)
            {
                Tile[,] moistureFractalTiles = terrainData.moistureMap.CreateFractalTiles();
                Texture2D tex = TextureGenerator.CreateMoistureMapTexture(terrainData.mapSize, terrainData.mapSize, ref moistureFractalTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/moistureMapBasicRenderer.png");
                moistureMapBasicRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (moistureMapFractalRenderer != null)
            {
                Tile[,] moistureFractalTiles = terrainData.moistureMap.CreateFractalTiles();
                Texture2D tex = TextureGenerator.CreateMoistureTypeMapTexture(terrainData.mapSize, terrainData.mapSize, ref moistureFractalTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/moistureMapFractalRenderer.png");
                moistureMapFractalRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            if (moistureMapAdjustedRenderer != null)
            {
                Texture2D tex = TextureGenerator.CreateMoistureTypeMapTexture(terrainData.mapSize, terrainData.mapSize, ref terrainData.mapTiles, ref terrainData.mapTiles, terrainData.outlineTerrainTypes);
                if (saveLoadTextures)
                    TextureGenerator.SaveTextureAsPNG(tex, "Resources/Textures/moistureMapAdjustedRenderer.png");
                moistureMapAdjustedRenderer.sharedMaterial.SetTexture("_MainTex", tex);
            }

            #endregion

        }

        /// <summary>
        /// Struct used to pass map information from threads.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        struct MapThreadInfo<T>
        {
            /// <summary>
            /// The parameter to send back to the callback handler.
            /// This is the result of the thread.
            /// </summary>
            public readonly T parameter;

            /// <summary>
            /// The handler desired to be notified of thread's results.
            /// </summary>
            public readonly Action<T> callback;

            /// <summary>
            /// Creates a map of the handler and the data that is the result of the thread.
            /// </summary>
            /// <param name="parameter">This is the result of the thread.</param>
            /// <param name="callback">The handler desired to be notified of thread's results.</param>
            public MapThreadInfo(T parameter, Action<T> callback)
            {
                this.parameter = parameter;
                this.callback = callback;
            }
        }
    }
}