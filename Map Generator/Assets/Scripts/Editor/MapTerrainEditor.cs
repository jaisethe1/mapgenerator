﻿using UnityEngine;
using UnityEditor;

namespace Noodle.MapGenerator
{
    /// <summary>
    /// Provides utility within editor to manually and automatically update the map and mesh.
    /// </summary>
    [CustomEditor(typeof(MapTerrain))]
    public class MapTerrainEditor : Editor
    {
        /// <summary>
        /// The edge to draw.
        /// </summary>
        private enum EdgeDrawMode
        {
            Top,
            Bottom,
            Left,
            Right
        }

        MapTerrain terrain = null;

        public override void OnInspectorGUI()
        {
            terrain = (MapTerrain)target;

            DrawDefaultInspector();

            if (GUILayout.Button("Generate"))
            {
                terrain.GetTerrainData(true);
            }

            if (GUILayout.Button("Draw Chunk Verts/Normals"))
            {
                DrawAllChunkVertsAndNormals();
            }

            if (GUILayout.Button("Delete Chunk Verts/Normals"))
            {
                for (int x = 0; x < terrain.TerrainSize; ++x)
                {
                    for (int y = 0; y < terrain.TerrainSize; ++y)
                    {
                        Vector2 chunkCoord = new Vector2(x, y);

                        if (terrain.TerrainChunkDictionary.ContainsKey(chunkCoord))
                        {
                            terrain.TerrainChunkDictionary[chunkCoord].MeshObject.transform.DestroyChildren();
                        }
                    }
                }
            }

            if (GUILayout.Button("Refresh Terrain Data"))
            {
                bool genMesh = terrain.generateMesh;
                terrain.generateMesh = false;

                terrain.GetTerrainData(true);

                terrain.generateMesh = genMesh;
            }

            if (GUILayout.Button("Draw Terrain Types To Generator Renderers"))
            {
                terrain.TerrainData.saveLoadTextures = true;
                terrain.TerrainData.createAllTextureForms = true;
                terrain.TerrainData.ignoreHeatMap = true;
                terrain.TerrainData.ignoreMoistureMap = true;
                MapTerrain.mapGenerator.CreateMapTextures(terrain.TerrainData);
                AssetDatabase.Refresh();
            }

            if (!Application.isPlaying)
                MapTerrain.mapGenerator.CheckThreadQueue();
        }

        /// <summary>
        /// Draws all of the verts/normals of all chunks.
        /// </summary>
        private void DrawAllChunkVertsAndNormals()
        {
            for (int x = 0; x < terrain.TerrainSize; ++x)
            {
                for (int y = 0; y < terrain.TerrainSize; ++y)
                {
                    Vector2 chunkCoord = new Vector2(x, y);

                    if (terrain.TerrainChunkDictionary.ContainsKey(chunkCoord))
                    {
                        terrain.TerrainChunkDictionary[chunkCoord].MeshObject.transform.DestroyChildren();

                        DrawChunkVertsAndNormals(terrain.TerrainChunkDictionary[chunkCoord], EdgeDrawMode.Top);
                        DrawChunkVertsAndNormals(terrain.TerrainChunkDictionary[chunkCoord], EdgeDrawMode.Bottom);
                        DrawChunkVertsAndNormals(terrain.TerrainChunkDictionary[chunkCoord], EdgeDrawMode.Left);
                        DrawChunkVertsAndNormals(terrain.TerrainChunkDictionary[chunkCoord], EdgeDrawMode.Right);
                    }
                }
            }
        }

        /// <summary>
        /// Draws a gizmo for the chunk's vertex and normal along a given edge based on draw mode.
        /// </summary>
        /// <param name="chunk">The chunk to draw.</param>
        /// <param name="drawMode">The edge to draw.</param>
        private void DrawChunkVertsAndNormals(MapTerrain.TerrainChunk chunk, EdgeDrawMode drawMode)
        {
            if (chunk.MeshData == null)
                return;

            MeshData chunkMeshData = chunk.MeshData;
            Transform chunkTrans = chunk.MeshObject.transform;            

            if (drawMode == EdgeDrawMode.Top)
            {
                int[] topVertIndex = chunkMeshData.topEdgeVertexIndexes;
                for (int iTop = 0; iTop < topVertIndex.Length; ++iTop)
                {
                    // transform vertex position to be relative to chunk's transform
                    Vector3 vertPos = chunkTrans.TransformPoint(chunkMeshData.vertices[topVertIndex[iTop]]);
                    Vector3 normalPos = vertPos + chunkMeshData.normals[topVertIndex[iTop]] * 2;

                    // create line gizmo to draw line from vert to the normal -> shows dir of normal
                    CreateLineGizmo(vertPos, normalPos, chunkTrans, Color.blue);
                }
            }
            else if (drawMode == EdgeDrawMode.Bottom)
            {
                int[] bottomVertIndex = chunkMeshData.bottomEdgeVertexIndexes;
                for (int iBot = 0; iBot < bottomVertIndex.Length; ++iBot)
                {
                    // transform vertex position to be relative to chunk's transform
                    Vector3 vertPos = chunkTrans.TransformPoint(chunkMeshData.vertices[bottomVertIndex[iBot]]);
                    Vector3 normalPos = vertPos + chunkMeshData.normals[bottomVertIndex[iBot]];

                    // create line gizmo to draw line from vert to the normal -> shows dir of normal
                    CreateLineGizmo(vertPos, normalPos, chunkTrans, Color.red);
                }
            }
            else if (drawMode == EdgeDrawMode.Left)
            {
                int[] leftVertIndex = chunk.MeshData.leftEdgeVertexIndexes;
                for (int iLeft = 0; iLeft < leftVertIndex.Length; ++iLeft)
                {
                    // transform vertex position to be relative to chunk's transform
                    Vector3 vertPos = chunkTrans.TransformPoint(chunkMeshData.vertices[leftVertIndex[iLeft]]);
                    Vector3 normalPos = vertPos + chunkMeshData.normals[leftVertIndex[iLeft]] * 2;

                    // create line gizmo to draw line from vert to the normal -> shows dir of normal
                    CreateLineGizmo(vertPos, normalPos, chunkTrans, Color.green);
                }
            }
            else if (drawMode == EdgeDrawMode.Right)
            {
                int[] rightVertIndex = chunk.MeshData.rightEdgeVertexIndexes;
                for (int iRight = 0; iRight < rightVertIndex.Length; ++iRight)
                {
                    // transform vertex position to be relative to chunk's transform
                    Vector3 vertPos = chunkTrans.TransformPoint(chunkMeshData.vertices[rightVertIndex[iRight]]);
                    Vector3 normalPos = vertPos + chunkMeshData.normals[rightVertIndex[iRight]];

                    // create line gizmo to draw line from vert to the normal -> shows dir of normal
                    CreateLineGizmo(vertPos, normalPos, chunkTrans, Color.yellow);
                }
            }
        }

        /// <summary>
        /// Creates a LineGizmo which can be used to draw a line from a vertex in the direction of the normal.
        /// </summary>
        /// <param name="vertPos">Position of the vertex.</param>
        /// <param name="normalPos">Position of vertex in direction of normal.</param>
        /// <param name="gizmoParent">The parent to attach gizmo GOs under.</param>
        /// <param name="colorOfGizmo">Color of gizmo line.</param>
        private void CreateLineGizmo(Vector3 vertPos, Vector3 normalPos, Transform gizmoParent, Color colorOfGizmo)
        {
            // create a GO which will be positioned at vertex position 
            GameObject vertPosGo = new GameObject("Vertex Base");
            vertPosGo.transform.parent = gizmoParent;
            vertPosGo.transform.position = vertPos;

            // create a GO which will be positioned in direction of normal from vertex position
            GameObject normalPosGo = new GameObject("Normal Dir");
            normalPosGo.transform.parent = gizmoParent;
            normalPosGo.transform.position = normalPos;

            // add LineGizmo to draw a line in the direction of normal
            Line_Gizmo lineGizmo = vertPosGo.AddComponent<Line_Gizmo>();
            lineGizmo.target = normalPosGo;
            lineGizmo.color = colorOfGizmo;
        }
    }

}