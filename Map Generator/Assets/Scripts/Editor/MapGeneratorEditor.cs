﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Noodle.MapGenerator;

namespace Noodle
{
    /// <summary>
    /// Provides utility within editor to manually and automatically update the map and mesh.
    /// </summary>
    [CustomEditor(typeof(MapGenerator.MapGenerator))]
    public class MapGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            MapGenerator.MapGenerator mapGen = (MapGenerator.MapGenerator)target;

            MapTerrain[] terrains = GameObject.FindObjectsOfType<MapTerrain>();

            if (DrawDefaultInspector())
            {
                // auto update (if enabled) the map data if the inspector has updated
                /*if (mapGen.autoUpdate)
                {
                    foreach(MapTerrain terrain in terrains)
                    {
                        terrain.CreateChunks();
                    }
                }*/
            }

            if (GUILayout.Button("Generate"))
            {
                foreach (MapTerrain terrain in terrains)
                {
                    terrain.GetTerrainData(true);
                }
            }

            /*if (GUILayout.Button("Save Terrain Data"))
            {
                TerrainDataAsset terrainAsset = ScriptableObjectUtil.CreateAsset<TerrainDataAsset>("Data/TerrainDataAssets/", "Test Terrain Data");
                terrainAsset.terrainData = mapGen.terrainDataModel;

                EditorUtility.SetDirty(terrainAsset);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }*/

            if (!Application.isPlaying)
                mapGen.CheckThreadQueue();
        }
    }

}