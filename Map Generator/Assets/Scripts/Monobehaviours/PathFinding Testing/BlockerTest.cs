﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

namespace Noodle
{
    [RequireComponent(typeof(SingleNodeBlocker))]
    public class BlockerTest : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var blockManager = GameObject.FindObjectOfType<BlockManager>();
            var blocker = GetComponent<SingleNodeBlocker>();
            blocker.manager = blockManager;
            blocker.BlockAtCurrentPosition();
        }
    }

}