﻿using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using BezierSolution;

public class Bezier_Spline : MonoBehaviour
{
    public Seeker seeker;

    public Transform start;
    public Transform end;

    public BezierSpline spline;

    public void Start()
    {
        seeker.StartPath(start.position, end.position, OnPathComplete);
    }

    public void OnPathComplete(Path path)
    {
        //ABPath path = ABPath.Construct(start.position, end.position, null);

        List<Vector3> pathVectors = path.vectorPath;
        List<GraphNode> pathNodes = path.path;
        int numNodes = pathVectors.Count;

        spline = new GameObject().AddComponent<BezierSpline>();
        spline.Initialize(numNodes);
        spline.loop = false;

        for (int i = 0; i < pathVectors.Count; ++i)
        {
            spline[i].position = pathVectors[i];
        }

        spline.ConstructLinearPath();
        //spline.AutoConstructSpline2();
    }

}