﻿
using UnityEngine;
using BezierSolution;
using Pathfinding;
using System.Collections.Generic;

[RequireComponent(typeof(LineRenderer))]
public class SplineLineRenderer : MonoBehaviour
{
    private LineRenderer lineRenderer;

    public Seeker seeker;

    public Transform start;
    public Transform end;

    public BezierSpline spline;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        spline = GetComponent<BezierSpline>();

        seeker.StartPath(start.position, end.position, OnPathComplete);
    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            // Do something with the object that was hit by the raycast.
            /*if (end != objectHit)
            {
                end = objectHit;
                seeker.StartPath(start.position, hit.point, OnPathComplete);
            }*/
            if(seeker.IsDone())
                seeker.StartPath(start.position, hit.point, OnPathComplete);
        }
    }

    public void Synchronize(int smoothness)
    {
        int numberOfPoints = spline.Count * Mathf.Clamp(smoothness, 1, 30);

        Vector3[] positions = new Vector3[numberOfPoints + 1];
        float lineStep = 1f / numberOfPoints;
        for (int i = 0; i < numberOfPoints; i++)
            positions[i] = spline.GetPoint(lineStep * i);

        positions[numberOfPoints] = spline.GetPoint(1f);

        lineRenderer.positionCount = positions.Length;
        lineRenderer.SetPositions(positions);
        lineRenderer.loop = spline.loop;
    }

    public void OnPathComplete(Path path)
    {
        //ABPath path = ABPath.Construct(start.position, end.position, null);

        List<Vector3> pathVectors = path.vectorPath;
        List<GraphNode> pathNodes = path.path;
        int numNodes = pathVectors.Count;

        spline.Initialize(numNodes);
        spline.loop = false;

        for (int i = 0; i < pathVectors.Count; ++i)
        {
            spline[i].position = pathVectors[i];
        }

        spline.AutoConstructSpline2();

        Synchronize(5);
    }
}
