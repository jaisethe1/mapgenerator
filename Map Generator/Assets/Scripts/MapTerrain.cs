﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Noodle.MapGenerator
{
    [ExecuteInEditMode()]
    /// <summary>
    /// A map terrain represents a collection of terrain chunks that when combined
    /// together create a full terrain map.
    /// </summary>
    public class MapTerrain : MonoBehaviour
    {
        public enum MapShaderType
        {
            GeneratedColorTexture,
            ColorShade
        }

        /// <summary>
        /// Represents the number of terrain chunks in each dimension.
        /// Assumes terrains are square.
        /// EX: Three = 3x3 terrain square.
        /// </summary>
        [SerializeField]
        [Range(1, 10)]
        private int terrainSize = 1;
        public int TerrainSize
        {
            get { return terrainSize; }
        }

        /// <summary>
        /// The size of each chunk of terrain.
        /// </summary>
        [SerializeField]
        [ReadOnly]
        private int chunkSize;

        [SerializeField]
        public MapShaderType mapShaderType = MapShaderType.GeneratedColorTexture;

        /// <summary>
        /// The material used to display the map with color shading.
        /// </summary>
        [SerializeField]
        public Material mapColorShadingMaterial;

        /// <summary>
        /// The material used to display the map with a generated texture.
        /// </summary>
        [SerializeField]
        public Material mapTextureMaterial;

        /// <summary>
        /// The generator which houses all the map data facility functions.
        /// </summary>
        public static MapGenerator mapGenerator;

        /// <summary>
        /// A map of 2D position to terrain chunk.
        /// </summary>
        private Dictionary<Vector2, TerrainChunk> terrainChunkDictionary = new Dictionary<Vector2, TerrainChunk>();
        public Dictionary<Vector2, TerrainChunk> TerrainChunkDictionary
        {
            get { return terrainChunkDictionary; }
        }

        /// <summary>
        /// The data defining this terrain chunk.
        /// </summary>
        [SerializeField]
        //[HideInInspector]
        private TerrainData terrainData;
        public TerrainData TerrainData
        {
            get { return this.terrainData; }
        }

        /// <summary>
        /// The flag to generate the terrain mesh or not.
        /// </summary>
        public bool generateMesh = false;

        private void OnEnable()
        {
            // become notified when a chunk mesh has been created
            TerrainChunk.onChunkMeshCreatedHandler += CanNormalizeChunkEdges;

            if (mapGenerator == null)
                mapGenerator = GameObject.FindObjectOfType<MapGenerator>();

            chunkSize = MapGenerator.MAP_CHUNK_SIZE - 1;

            //GetTerrainData(true);
        }

        /// <summary>
        /// Gets the terrain data from the map generator (threaded).
        /// </summary>
        /// <param name="forceUpdate">True to force update a non-null terrain data to get the latest. 
        /// Useful for Editor reloads that break references and can be used to refresh it. </param>
        public void GetTerrainData(bool forceUpdate = false)
        {
            // get chunk's terrain data
            if (terrainData == null || forceUpdate)
                mapGenerator.StartTerrainDataGeneratorThread(terrainSize, OnTerrainDataReceived);
            else
                OnTerrainDataReceived(this.terrainData);
        }

        /// <summary>
        /// Handler notified when terrain data thread has concluded.
        /// Goes on to create the chunks of the terrain mesh.
        /// </summary>
        /// <param name="terrainData">The newly acquired terrain data that was generated.</param>
        public void OnTerrainDataReceived(TerrainData terrainData)
        {
            this.terrainData = terrainData;

            if(generateMesh)
                CreateChunks(true);
        }

        /// <summary>
        /// Creates all of the chunks of the terrain based on the size of the terrain.
        /// </summary>
        private void CreateChunks(bool forceUpdate = false)
        {
            // prevent destroying unless we need to and allow updating of terrain chunks so we can keep seeds.
            // prevent duplication and unused objects from polluting scene
            if(transform.childCount != terrainChunkDictionary.Count || forceUpdate)
                DeleteAndClearChunks();

            // create a chunk in each dimension relative to the number of desired chunks.
            for (int x = 0; x < terrainSize; ++x)
            {
                for(int y = 0; y < terrainSize; ++y)
                {
                    Vector2 chunkCoord = new Vector2(x, y);

                    // offset the terrain chunk position based on the terrain chunk's 'index' and the size of the chunk
                    if (!terrainChunkDictionary.ContainsKey(chunkCoord))
                    {
                        // determine the material to assign to the terrain chunk based on the
                        // desired shading type.
                        Material mapMaterial;
                        {
                            switch(mapShaderType)
                            {
                                case MapShaderType.GeneratedColorTexture:
                                    mapMaterial = mapTextureMaterial;
                                    break;
                                case MapShaderType.ColorShade:
                                    mapMaterial = mapColorShadingMaterial;
                                    break;
                                default:
                                    mapMaterial = mapTextureMaterial;
                                    break;
                            }
                        }
                        TerrainChunk newChunk = new TerrainChunk(chunkCoord, chunkSize, transform, mapMaterial, terrainData, mapShaderType);
                        terrainChunkDictionary.Add(chunkCoord, newChunk);
                    }
                }
            }
        }

        /// <summary>
        /// Checks to see if it is the right time to normalize all
        /// chunk edges. The 'right time' is once all chunks have 
        /// received their mesh data.
        /// 
        /// This will normalize chunk edges if it is the right time, 
        /// then it will create the meshes with the newly 'normalized' data.
        /// </summary>
        private void CanNormalizeChunkEdges()
        {
            // once all meshes have been created, then we can normalize the edges, but not until then
            for (int x = 0; x < terrainSize; ++x)
            {
                for (int y = 0; y < terrainSize; ++y)
                {
                    Vector2 chunkCoord = new Vector2(x, y);

                    if (terrainChunkDictionary.ContainsKey(chunkCoord))
                    {
                        if (!terrainChunkDictionary[chunkCoord].HasReceivedMeshData)
                            return;
                    }
                }
            }

            NormalizeChunkEdges();

            CreateTerrainChunkMeshes();
        }

        /// <summary>
        /// Creates all of the terrain chunks' meshes.
        /// </summary>
        private void CreateTerrainChunkMeshes()
        {
            for (int x = 0; x < terrainSize; ++x)
            {
                for (int y = 0; y < terrainSize; ++y)
                {
                    Vector2 chunkCoord = new Vector2(x, y);

                    if (terrainChunkDictionary.ContainsKey(chunkCoord))
                    {
                        terrainChunkDictionary[chunkCoord].CreateTerrainMesh(ref terrainData);
                    }
                }
            }
        }

        /// <summary>
        /// Normalizes the edges of adjacent chunks to prevent seems from appearing because 
        /// the normals that are calculated without consideration for surrounding chunks.
        /// </summary>
        private void NormalizeChunkEdges()
        {
            // normal-fy the edge vertices between connecting chunks to remove the seem the appears due to floating point calculations.
            ///     
            ///     EXAMPLE: 3x3 terrain
            ///     
            ///     [0,2]   [1,2]   [2,2]
            ///     [0,1]   [1,1]   [2,1]
            ///     [0,0]   [1,0]   [2,0]
            /// 
            ///     Starting at [0,0] the top chunk [0,1] and right chunk [1,0] need to 
            ///     be edge aligned along their bottom and left edges, respectfully.
            ///     Repeat this process for all chunks using the left-bottom most
            ///     chunks as sources.
            /// 
            Vector2 currentChunkCoord = Vector2.zero;
            Vector2 topChunkCoord = Vector2.zero;
            Vector2 rightChunkCoord = Vector2.zero;
            for (int x = 0; x < terrainSize; ++x)
            {
                for (int y = 0; y < terrainSize; ++y)
                {
                    // sanity check to ensure the source terrain chunk exists.
                    // it should, but checking never hurts.
                    currentChunkCoord.x = x;
                    currentChunkCoord.y = y;
                    if (!terrainChunkDictionary.ContainsKey(currentChunkCoord))
                        continue;

                    topChunkCoord.x = x;
                    topChunkCoord.y = y + 1;

                    rightChunkCoord.x = x + 1;
                    rightChunkCoord.y = y;

                    TerrainChunk sourceChunk = terrainChunkDictionary[currentChunkCoord];

                    if (terrainChunkDictionary.ContainsKey(topChunkCoord))
                    {
                        TerrainChunk topChunk = terrainChunkDictionary[topChunkCoord];
                        TerrainChunk.NormalizeChunkEdge(ref sourceChunk, ref topChunk, TerrainChunk.NormalizeChunkSide.Top);
                        terrainChunkDictionary[topChunkCoord] = topChunk;
                    }

                    if (terrainChunkDictionary.ContainsKey(rightChunkCoord))
                    {
                        TerrainChunk rightChunk = terrainChunkDictionary[rightChunkCoord];
                        TerrainChunk.NormalizeChunkEdge(ref sourceChunk, ref rightChunk, TerrainChunk.NormalizeChunkSide.Right);
                        terrainChunkDictionary[rightChunkCoord] = rightChunk;
                    }
                }
            }
        }

        /// <summary>
        /// Delete any already created terrain chunks and clear the dictionary references.
        /// This can be used to 'reset the stage' for creating a new set of terrain chunks.
        /// </summary>
        private void DeleteAndClearChunks() 
        {
            transform.DestroyChildren();

            terrainChunkDictionary.Clear();
        }

        /// <summary>
        /// Represents a chunk of the whole terrain.
        /// </summary>
        [Serializable]
        public class TerrainChunk
        {
            /// <summary>
            /// The side of the source chunk to normalize to the destination chunk.
            /// </summary>
            public enum NormalizeChunkSide
            {
                Top,
                Right
            }

            /// <summary>
            /// A handler to call when a chunk mesh has been created.
            /// </summary>
            public static event Action onChunkMeshCreatedHandler;

            /// <summary>
            /// The actual mesh object representing the terrain chunk in the scene.
            /// </summary>
            private GameObject meshObject;
            public GameObject MeshObject
            {
                get { return meshObject; }
            }

            /// <summary>
            /// The terrain mesh's renderer.
            /// </summary>
            private MeshRenderer meshRenderer;

            /// <summary>
            /// The terrain mesh's filter.
            /// </summary>
            private MeshFilter meshFilter;

            /// <summary>
            /// The terrain mesh's collider.
            /// </summary>
            private MeshCollider meshCollider;

            /// <summary>
            /// A reference to the map's material.
            /// </summary>
            private Material mapMaterial;

            /// <summary>
            /// The size of the mesh chunk.
            /// </summary>
            private int meshChunkSize;

            /// <summary>
            /// The 0-index <int> (x, y) position of the mesh. 
            /// This is used to scale indicies (in conjunction with chunk size) for use 
            /// with large tile-maps to get the data in chunks. 
            /// </summary>
            private Vector2 indexPosition;

            /// <summary>
            /// The type of shading methodology to use for rendering the terrain chunk.
            /// </summary>
            private MapShaderType mapShaderType;

            /// <summary>
            /// The mesh's data based on terrain height map.
            /// </summary>
            private MeshData meshData;
            public MeshData MeshData
            {
                get { return meshData; }
            }

            /// <summary>
            /// The flag indicating if the chunk has received its mesh data yet or not.
            /// </summary>
            private bool hasReceivedMeshData = false;
            public bool HasReceivedMeshData
            {
                get { return hasReceivedMeshData; }
            }

            /// <summary>
            /// Creates a terrain chunk object at the given position.
            /// </summary>
            /// <param name="coord">The indexed position of the terrain chunk.</param>
            /// <param name="chunkSize">The size of the chunk to be created.</param>
            /// <param name="parent">The parent of the chunk (so not to pollute the heirarchy view). </param>
            /// <param name="mapMaterial">The material to be cloned that will display the mesh texture. </param>
            /// <param name="terrainData">The reference to the terrain data this chunk is to use.</param>
            /// <param name="mapShaderType">The type of shading methodology to use for rendering the terrain chunk.</param>
            public TerrainChunk(Vector2 coord, int chunkSize, Transform parent, Material mapMaterial, TerrainData terrainData, MapShaderType mapShaderType)
            {
                this.mapShaderType = mapShaderType;
                this.mapMaterial = new Material(mapMaterial);           

                meshChunkSize = chunkSize;

                indexPosition = coord;
                Vector2 position2D = coord * chunkSize;
                Vector3 position3D = new Vector3(position2D.x, 0, position2D.y);

                // create and position terrain chunk mesh so it's ready for mesh creation
                string meshObjectName = string.Format("Terrain Chunk ({0}, {1})", position2D.x, position2D.y);
                meshObject = new GameObject(meshObjectName);
                {
                    meshRenderer = meshObject.AddComponent<MeshRenderer>();
                    meshFilter = meshObject.AddComponent<MeshFilter>();
                    meshCollider = meshObject.AddComponent<MeshCollider>();

                    if (Application.isPlaying)
                        meshRenderer.material = this.mapMaterial;
                    else
                        meshRenderer.sharedMaterial = this.mapMaterial;
                }
                if (parent != null)
                    meshObject.transform.parent = parent;

                meshObject.transform.localPosition = position3D;

                mapGenerator.StartMeshDataGeneratorThread(terrainData, indexPosition, chunkSize+1, OnMeshDataReceived);
            }

            /// <summary>
            /// Called when generation thread has completed.
            /// It creates the mesh from the received mesh data along with the 
            /// terrains associated texture.
            /// </summary>
            /// <param name="meshData">The mesh data created by the generator thread.</param>
            private void OnMeshDataReceived(MeshData meshData)
            {
                this.meshData = meshData;

                hasReceivedMeshData = true;
                onChunkMeshCreatedHandler();
            }

            /// <summary>
            /// Creates the terrain's mesh and texture using the terrain data.
            /// </summary>
            /// <param name="terrainData">The terrain data which specifies how the texture should be created.</param>
            public void CreateTerrainMesh(ref TerrainData terrainData)
            {
                Mesh terrainChunkMesh = meshData.CreateMesh();
                SetMaterialProperties(ref this.mapMaterial, this, ref terrainData);

                if (Application.isPlaying)
                {
                    
                    meshFilter.mesh = terrainChunkMesh;
                    meshCollider.sharedMesh = terrainChunkMesh;
                }
                else
                {
                    meshFilter.sharedMesh = terrainChunkMesh;
                    meshCollider.sharedMesh = terrainChunkMesh;
                }
            }

            /// <summary>
            /// Aligns the normals of an edge of a chunk to the normals of a source chunk.
            /// Also equalize the heights of the edge vertices so they meet up exactly.
            /// This is used to prevent seams due to floating-point issues.
            /// </summary>
            /// <param name="sourceChunk">The source chunk used to reference normals which are transferred to other chunk.</param>
            /// <param name="chunkToNormalize">The chunk that is getting its edge normals adjusted.</param>
            /// <param name="sideToNormalize">The side of the source chunk which is getting transfered to chunk in question.</param>
            ///      
            ///     EXAMPLE: 3x3 terrain
            ///     
            ///     [0,2]   [1,2]   [2,2]
            ///     [0,1]   [1,1]   [2,1]
            ///     [0,0]   [1,0]   [2,0]
            /// 
            ///     Starting at [0,0] the top chunk [0,1] and right chunk [1,0] need to 
            ///     be edge aligned along their bottom and left edges, respectfully.
            ///     Repeat this process for all chunks using the left-bottom most
            ///     chunks as sources.
            /// 
            public static void NormalizeChunkEdge(ref TerrainChunk sourceChunk, ref TerrainChunk chunkToNormalize, NormalizeChunkSide sideToNormalize)
            {
                // the num of verts = squared size, so need to root the size to get the num verts per line
                int verticesPerLine = (int)Mathf.Sqrt(sourceChunk.meshData.vertices.Length);
                
                if (sideToNormalize == NormalizeChunkSide.Top)
                {
                    // by normalizing the top of the source, we need to transfer those values to the bottom
                    // normals of the destination chunk
                    Vector3[] sourceNormals = sourceChunk.meshData.normals;
                    int[] sourceTopIndexes = sourceChunk.meshData.topEdgeVertexIndexes;
                    int[] destBotIndexes = chunkToNormalize.meshData.bottomEdgeVertexIndexes;

                    int sourceVertIndex = 0;
                    int destVertIndex = 0;
                    for (int i = 0; i < sourceTopIndexes.Length; ++i)
                    {
                        // replace destination's normal value with source normal value.
                        sourceVertIndex = sourceTopIndexes[i];
                        destVertIndex = destBotIndexes[i];
                        chunkToNormalize.meshData.normals[destVertIndex] = sourceNormals[sourceVertIndex];

                        // align height at the vertex
                        Vector3 destVertPos = chunkToNormalize.meshData.vertices[destVertIndex];
                        destVertPos.y = sourceChunk.meshData.vertices[sourceVertIndex].y;
                        chunkToNormalize.meshData.vertices[destVertIndex] = destVertPos;
                    }
                }

                if(sideToNormalize == NormalizeChunkSide.Right)
                {
                    // by normalizing the right of the source, we need to transfer those values to the left
                    // normals of the destination chunk
                    Vector3[] sourceNormals = sourceChunk.meshData.normals;
                    Vector3[] sourceVerts = sourceChunk.meshData.vertices;
                    int[] sourceTopIndexes = sourceChunk.meshData.rightEdgeVertexIndexes;
                    int[] destBotIndexes = chunkToNormalize.meshData.leftEdgeVertexIndexes;

                    int sourceVertIndex = 0;
                    int destVertIndex = 0;
                    for (int i = 0; i < sourceTopIndexes.Length; ++i)
                    {
                        // replace destination's normal value with source normal value.
                        sourceVertIndex = sourceTopIndexes[i];
                        destVertIndex = destBotIndexes[i];
                        chunkToNormalize.meshData.normals[destVertIndex] = sourceNormals[sourceVertIndex];

                        // align height at the vertex
                        Vector3 destVertPos = chunkToNormalize.meshData.vertices[destVertIndex];
                        destVertPos.y = sourceChunk.meshData.vertices[sourceVertIndex].y;
                        chunkToNormalize.meshData.vertices[destVertIndex] = destVertPos;
                    }
                }
            }

            /// <summary>
            /// Sets any material/shader properties that are required based on the data that defines
            /// the terrain and the desired shader type.
            /// </summary>
            /// <param name="mapMaterial">The material to set the properties of so it will render the terrain chunk as desired.</param>
            /// <param name="terrainChunk">The terrain chunk the material is rendering</param>
            /// <param name="terrainData">The reference to the terrain data that defines the details of the terrain chunk.</param>
            public static void SetMaterialProperties(ref Material mapMaterial, TerrainChunk terrainChunk, ref TerrainData terrainData)
            {
                if (terrainChunk.mapShaderType == MapShaderType.ColorShade)
                {
                    float minHeight = terrainData.MinHeight;
                    float maxHeight = terrainData.MaxHeight;
                    Color[] terrainColors = terrainData.terrainTypesConfig.TerrainColors;

                    // Adjust the terrain height thresholds based on the terrain's height curve
                    // we do this because the verts of the mesh are scaled by this and cause
                    // discoloration if not done.
                    float[] terrainHeights = terrainData.terrainTypesConfig.TerrainHeightThresholds;
                    for (int i = 0; i < terrainHeights.Length; ++i)
                    {
                        terrainHeights[i] = terrainData.heightCurve.Evaluate(terrainHeights[i]);
                    }

                    mapMaterial.SetInt("baseColorCount", terrainColors.Length);
                    mapMaterial.SetColorArray("baseColors", terrainColors);
                    mapMaterial.SetFloatArray("baseHeights", terrainHeights);

                    mapMaterial.SetFloat("minHeight", minHeight);
                    mapMaterial.SetFloat("maxHeight", maxHeight);
                }
                else if (terrainChunk.mapShaderType == MapShaderType.GeneratedColorTexture)
                {
                    Texture2D terrainChunkTexture = terrainData.CreateColoredHeightMapTextureForChunk(terrainChunk.indexPosition, terrainChunk.meshChunkSize - 1);
                    mapMaterial.SetTexture("_MainTex", terrainChunkTexture);
                }
            }
        }
    }
}