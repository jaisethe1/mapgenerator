// this files is automatically generated so please don't modify! 

public enum TerrainTypes
{
	DeepWater = 1,
	ShallowWater = 2,
	Sand = 3,
	Grass = 4,
	Forest = 5,
	Rock = 6,
	Snow = 7,
	}