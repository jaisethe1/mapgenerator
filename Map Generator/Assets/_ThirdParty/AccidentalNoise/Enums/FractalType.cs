namespace AccidentalNoise
{
    public enum FractalType
    {
        FRACTIONALBROWNIANMOTION,   // some ponds
        RIDGEDMULTI,                // fractal spiral, extremes
        BILLOW,                     // many islands / river-esque
        MULTI,                      // islands, more water than land
        HYBRIDMULTI
    }
}